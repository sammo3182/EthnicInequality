clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

set more off	
	
cd "E:\Dropbox_sync\Dropbox\Instruction\Research paper\EthnicInequality"

log using Hu_ethnicity_beforesubmit.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Ethnic inequality:generation and conflicts
*AUTHOR:	Yue Hu, University of Iowa
*MACHINE:	PC Win8.1, running Stata/SE 13.1
*FILE:		"Hu_ethnicity_aftermidwest.do"
*LOG:		"Hu_ethnicity_aftermidwest.log"
*DATA:		CLDS
*SOURCES:	2012 年中国劳动力动态调查
*OUTPUT:	
*		TABLES/
*		GRAPHS/

use ethnicity_mod2.dta, clear
svyset _n [pw=wpp]

g working=0 
replace working=1 if I3a_13==1

gen islam =.
replace islam = 1 if I7_1 == 6
replace islam = 0 if I7_1 != 6 & I7_1 != .

g han = 1 if F1_1_101_5 == 1
g hui = 1 if F1_1_101_5 == 4
g wei = 1 if F1_1_101_5 == 6

replace han = 0 if F1_1_101_5 != 1
replace hui = 0 if F1_1_101_5 != 4
replace wei = 0 if F1_1_101_5 != 6

gen migrant = 1 - localhukou

g ethnicity3=.
replace ethnicity3=1 if han==1
replace ethnicity3=2 if hui==1
replace ethnicity3=3 if wei==1
lab def ethnicity3 1"han" 2"hui" 3"wei"
lab val ethnicity3 ethnicity3


eststo sem_full_1a9:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility@1 -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o2, )(edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, )  (nonagr -> logincom_p, )(promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent pw(wpp)  nrtolerance(1e-4) tolerance(1e-4) ltolerance(1e-5)  tech(bhhh dfp bfgs)
	
esttab sem_full_1a9 using sem_sem_aftermw_nojobchange.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.05 ** 0.01 *** 0.001) replace nonumbers ///
	title(SEM with Fixed Effect) 


eststo sem_full_1a10:gsem (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility@1 -> nonagr, family(binomial) link(logit))  (mobility -> promotion_o2, )(edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, )  (nonagr -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent nrtolerance(1e-3) tolerance(1e-3) ltolerance(1e-4)
	
esttab sem_full_1a10 using sem_sem_aftermw_nojobchange_nolang.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.05 ** 0.01 *** 0.001) replace nonumbers ///
	title(SEM with Fixed Effect) 
	
	
* Analysis on the effect of language
local lang_set mandarinII hui wei age edu_dur female ccp urbanhukou migrant islam
local nolang_set hui wei age edu_dur female ccp urbanhukou migrant islam

factor logincom_p nonagr promotion_o2, ipf
rotate , blank(.4)
predict mobility

foreach var of varlist mobility logincom_p promotion_o2 {
	eststo: quiet reg `var' `nolang_set' if ethnicity3!=. & working==1 & occu_ty_com != 4 & occu_ty_com != 6
	eststo: quiet reg `var' `lang_set' if ethnicity3!=. & working==1 & occu_ty_com != 4 & occu_ty_com != 6
}

esttab using ".\language_effect.rtf", replace nonumbers compress b(3) onecell ///
	se stats(N F df_m df_r r2 r2_a, layout(@ `""@ (@, @)""' @ @ )) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Only Han Hui Wei + Having job(I3a_13) + Not self-employed or job type is 'other'")
eststo clear


foreach var of varlist mobility logincom_p promotion_o2 {
	eststo: quiet reg `var' `nolang_set' if ethnicity3!=. & working==1 & occu_ty_com < 4
	eststo: quiet reg `var' `lang_set' if ethnicity3!=. & working==1 & occu_ty_com < 4
}

esttab using ".\language_effect.rtf", append nonumbers compress b(3) onecell ///
	se stats(N F df_m df_r r2 r2_a, layout(@ `""@ (@, @)""' @ @ )) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Only Han Hui Wei + Having job(I3a_13) + Not self-employed,agriculture or job type is 'other'")
eststo clear


foreach var of varlist mobility logincom_p promotion_o2 {
	eststo: quiet reg `var' `nolang_set' if ethnicity3!=. & working==1 
	eststo: quiet reg `var' `lang_set' if ethnicity3!=. & working==1 
}

eststo: quiet logit nonagr `nolang_set' if ethnicity3!=. & working==1
eststo: quiet logit nonagr `lang_set' if ethnicity3!=. & working==1

esttab using ".\language_effect.rtf", append nonumbers compress b(3) onecell ///
	se transform(ln*: exp(@) exp(@)) ///
	stats(N ll chi2 df_m r2_p, layout(@ @ `""@ (@)""' @)) /// @ represents the position of a parameter.
 	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Only Han Hui Wei + Having job(I3a_13)")
eststo clear



local lang_set mandarinII hui wei age female ccp migrant islam

foreach var of varlist age edu_dur female ccp urbanhukou migrant islam  {
	reg promotion_o2 `lang_set' `var' if ethnicity3!=. & working==1 & occu_ty_com == 1
	reg promotion_o2 `lang_set' `var' if ethnicity3!=. & working==1 & occu_ty_com == 2
	reg promotion_o2 `lang_set' `var' if ethnicity3!=. & working==1 & occu_ty_com == 3
}


local lang_set mandarinII hui wei 

foreach var of varlist age edu_dur female ccp urbanhukou migrant islam  {
	reg promotion_o2 `lang_set' `var' if ethnicity3!=. & working==1 & occu_ty_com == 1
	reg promotion_o2 `lang_set' `var' if ethnicity3!=. & working==1 & occu_ty_com == 2
	reg promotion_o2 `lang_set' `var' if ethnicity3!=. & working==1 & occu_ty_com == 3
}


// age Gov: no; SOE: no; Pri: no
// edu Gov: y; SOE: y; Pri: no
// female Gov: y; SOE: no; Pri: no
// ccp Gov: y; SOE: no; Pri: no
// urbanhukou Gov: y; SOE: y; Pri: no
// migrant Gov: y; SOE: no; Pri: no
// islam Gov: y; SOE: no; Pri: no

// 结论：
// 1. 政府工作比较公平：对少数民族没有差异
// 2. 在国企、私人：对少数民族存在差异，而在国企中这种差异会随着modernization程度变化而减小
// 3. 在私企中均不管用，而私企基本是 其他两种工作岗位的两倍


* Solution 1: large sample
local lang_set mandarinII hui wei age edu_dur female ccp urbanhukou migrant
local nolang_set hui wei age edu_dur female ccp urbanhukou migrant

foreach var of varlist mobility logincom_p promotion_o2 {
	eststo: quiet mixed `var' `nolang_set' if ethnicity3!=. & working==1 || PROVINCE:  
	eststo: quiet mixed `var' `lang_set'  if ethnicity3!=. & working==1 || PROVINCE:
}

eststo: quiet melogit nonagr `nolang_set' if ethnicity3!=. & working==1 || PROVINCE: 
eststo: quiet melogit nonagr `lang_set' if ethnicity3!=. & working==1 || PROVINCE: 

esttab using ".\language_effect1023.rtf", replace nonumbers compress b(3) onecell ///
	se transform(ln*: exp(@) exp(@)) ///
	stats(N ll) /// @ represents the position of a parameter.
 	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Solution 1: Only Han Hui Wei + Having job(I3a_13)")
eststo clear

local lang_set mandarinII hui wei age edu_dur female ccp urbanhukou migrant islam
local nolang_set hui wei age edu_dur female ccp urbanhukou migrant islam

foreach var of varlist mobility logincom_p promotion_o2 {
	eststo: quiet mixed `var' `nolang_set' if ethnicity3!=. & working==1 || PROVINCE:  
	eststo: quiet mixed `var' `lang_set'  if ethnicity3!=. & working==1 || PROVINCE:
}

eststo: quiet melogit nonagr `nolang_set' if ethnicity3!=. & working==1 || PROVINCE: 
eststo: quiet melogit nonagr `lang_set' if ethnicity3!=. & working==1 || PROVINCE: 

esttab using ".\language_effect1023.rtf", append nonumbers compress b(3) onecell ///
	se transform(ln*: exp(@) exp(@)) ///
	stats(N ll) /// @ represents the position of a parameter.
 	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Elaborate: Only Han Hui Wei + Having job(I3a_13)")
eststo clear


local islam_set mandarinII hui wei age edu_dur female ccp urbanhukou migrant islam
local noislam_set mandarinII hui wei age edu_dur female ccp urbanhukou migrant 

eststo: quiet mixed promotion_o2 `noislam_set' `var' if ethnicity3!=. & working==1 || PROVINCE: 
eststo: quiet mixed promotion_o2 `islam_set' `var' if ethnicity3!=. & working==1 || PROVINCE: 

esttab using ".\language_effect1023.rtf", append nonumbers compress b(3) onecell ///
	se transform(ln*: exp(@) exp(@)) ///
	stats(N ll) /// @ represents the position of a parameter.
 	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Hui becomes insig without islam")
eststo clear



* Solution 2: small sample
local lang_set mandarinII hui wei age edu_dur female ccp urbanhukou migrant
local nolang_set hui wei age edu_dur female ccp urbanhukou migrant

foreach var of varlist mobility logincom_p {
	eststo: quiet mixed `var' `nolang_set' if ethnicity3!=. & working==1 || PROVINCE:  
	eststo: quiet mixed `var' `lang_set'  if ethnicity3!=. & working==1 || PROVINCE:
}

eststo: quiet mixed promotion_o2 `nolang_set' if ethnicity3!=. & working==1 & occu_ty_com < 4|| PROVINCE:  
eststo: quiet mixed promotion_o2 `lang_set'  if ethnicity3!=. & working==1 & occu_ty_com < 4|| PROVINCE:

eststo: quiet melogit nonagr `nolang_set' if ethnicity3!=. & working==1 || PROVINCE: 
eststo: quiet melogit nonagr `lang_set' if ethnicity3!=. & working==1 || PROVINCE: 

esttab using ".\language_effect1023.rtf", append nonumbers compress b(3) onecell ///
	se transform(ln*: exp(@) exp(@)) ///
	stats(N ll) /// @ represents the position of a parameter.
 	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Solution 1: Only Han Hui Wei + Having job(I3a_13) (Promotion: - Self Employed - Agriculture - Other)")
eststo clear


local islam_set mandarinII hui wei age edu_dur female ccp urbanhukou migrant islam
local noislam_set mandarinII hui wei age edu_dur female ccp urbanhukou migrant 

eststo: quiet mixed promotion_o2 `noislam_set' `var' if ethnicity3!=. & working==1 & occu_ty_com < 4|| PROVINCE: 
eststo: quiet mixed promotion_o2 `islam_set' `var' if ethnicity3!=. & working==1 & occu_ty_com < 4|| PROVINCE: 

esttab using ".\language_effect1023.rtf", append nonumbers compress b(3) onecell ///
	se transform(ln*: exp(@) exp(@)) ///
	stats(N ll) /// @ represents the position of a parameter.
 	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Hui becomes insig without islam")
eststo clear

// age Gov: no; SOE: no; Pri: no
// edu Gov: n; SOE: no; Pri: no
// female Gov: n; SOE: no; Pri: no
// ccp Gov: n; SOE: no; Pri: no
// urbanhukou Gov: n; SOE: no; Pri: no
// migrant Gov: n; SOE: no; Pri: no
// islam Gov: n; SOE: y; Pri: y


eststo: quiet logit nonagr `nolang_set' if ethnicity3!=. & working==1
eststo: quiet logit nonagr `lang_set' if ethnicity3!=. & working==1
eststo: quiet logit nonagr `lang_set_mod' if ethnicity3!=. & working==1

esttab using ".\language_effect1022.rtf", append nonumbers compress b(3) onecell ///
	se transform(ln*: exp(@) exp(@)) ///
	stats(N ll chi2 df_m r2_p, layout(@ @ `""@ (@)""' @)) /// @ represents the position of a parameter.
 	star(* 0.10 ** 0.05 *** 0.01) ///
	title("Solution 2: Only Han Hui Wei + Having job(I3a_13)")
eststo clear

estpost tab occu_ty_com if occu_ty_com < 4
esttab using ".\language_effect1022.rtf", append cells("b(label(Frequency))")  ///
  varlabels(, blist(Total "{hline @width}{break}"))      ///
  nonumber nomtitle noobs
