clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

cd "E:\Dropbox_sync\Dropbox\Instruction\Research paper\EthnicInequality"

log using Hu_ethnicity_aftermidwest.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Ethnic inequality:generation and conflicts
*AUTHOR:	Yue Hu, University of Iowa
*MACHINE:	PC Win8.1, running Stata/SE 13.1
*FILE:		"Hu_ethnicity_aftermidwest.do"
*LOG:		"Hu_ethnicity_aftermidwest.log"
*DATA:		CLDS
*SOURCES:	2012 年中国劳动力动态调查
*OUTPUT:	
*		TABLES/
*		GRAPHS/

use ethnicity_mod2.dta, clear
svyset _n [pw=wpp]

/*Descriptive*/
gen islam =.
replace islam = 1 if I7_1 == 6
replace islam = 0 if I7_1 != 6 & I7_1 != .

gen hhw = ethnicity if ethnicity == 1 | ethnicity == 4 | ethnicity == 6 // nominal variable for ethnicity
lab def hhw 1"han" 4"hui" 6"wei"
lab val hhw hhw

gen hanhui = .
replace hanhui = 0 if hhw == 1
replace hanhui = 1 if hhw == 4  //binary variable for han(0) vs. hui(1)


svy: mean edu_dur, over(hhw)
lincom [edu_dur]wei - [edu_dur]han 
lincom [edu_dur]hui - [edu_dur]han


svy: mean mandarinII, over(hhw)
lincom [mandarinII]wei - [mandarinII]han 
lincom [mandarinII]hui - [mandarinII]han


svy: mean occu_change, over(hhw)
lincom [occu_change]wei - [occu_change]han 
lincom [occu_change]hui - [occu_change]han


svy: mean promotion_o4, over(hhw)
lincom [promotion_o4]wei - [promotion_o4]han 
lincom [promotion_o4]hui - [promotion_o4]han


sum logincom_p nonagr promotion_o2 mandarinII hui wei han edu_dur age female ccp urbanhukou migrant islam PROVINCE if ethnicity3!=. & working==1


/*Tang laoshi's model*/
g mandarin_t=(5-I10_9)/4*100

g working=0 
replace working=1 if I3a_13==1

g han = 1 if F1_1_101_5 == 1
g hui = 1 if F1_1_101_5 == 4
g wei = 1 if F1_1_101_5 == 6

replace han = 0 if F1_1_101_5 != 1
replace hui = 0 if F1_1_101_5 != 4
replace wei = 0 if F1_1_101_5 != 6

gen migrant = 1 - localhukou

g ethnicity3=.
replace ethnicity3=1 if han==1
replace ethnicity3=2 if hui==1
replace ethnicity3=3 if wei==1
lab def ethnicity3 1"han" 2"hui" 3"wei"
lab val ethnicity3 ethnicity3

g weiman=wei*mandarin_t
g huiman=hui*mandarin_t

eststo ochange_t: menbreg occu_change mandarin hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE: ,dif
eststo nonagr_t: melogit nonagr mandarin hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE: 
eststo jobup_t: meologit promotion_o2 mandarin hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE:


g weimanII=wei*mandarinII
g huimanII=hui*mandarinII

eststo ochange_t: menbreg occu_change mandarinII hui wei weimanII huimanII edu_dur age female ccp logincom_p urbanhukou localhukou islam [pw = wpp] if ethnicity3!=. & working==1 ||PROVINCE:,  technique(dfp)

eststo nonagr_t: melogit nonagr mandarinII hui wei weimanII huimanII edu_dur age female ccp logincom_p urbanhukou localhukou islam [pw = wpp] if ethnicity3!=. & working==1 ||PROVINCE:

eststo jobup_t: meologit promotion_o2 mandarinII hui wei weimanII huimanII edu_dur age female ccp logincom_p urbanhukou localhukou islam [pw = wpp] if ethnicity3!=. & working==1 ||PROVINCE:, dif

eststo jobup2_t: meologit promotion_o1 mandarinII hui wei weimanII huimanII edu_dur age female ccp logincom_p urbanhukou localhukou islam [pw = wpp] if ethnicity3!=. & working==1 ||PROVINCE:, dif


esttab ochange_t nonagr_t jobup_t jobup2_t using midwest_t_corrected.csv, ///
	se transform(ln*: exp(@) exp(@)) ///
    eqlabels("" "sd(_cons: State)" "sd(Residual)", none) ///
	varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll chi2 df_m p chi2_c df_c p_c, ///
	  layout(@ @ `""@ (@)""' @ `""@ (@)""' @)) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonumbers ///
	title(Tang laoshi's Models) ///
	mtitles("Job Change(Han vs. Uygur)" "Non-Agriculture" "Promotion (time)" "Promotion(level)")

	
eststo ochange_t2: menbreg occu_change mandarinII hui wei weimanII huimanII edu_dur age female ccp   urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE:, technique(dfp)

eststo nonagr_t2: melogit nonagr mandarinII hui wei weimanII huimanII edu_dur age female ccp   urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE: 

eststo jobup_t2: meologit promotion_o2 mandarinII hui wei weimanII huimanII edu_dur age female ccp   urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE:, dif

eststo jobup2_t2: meologit promotion_o1 mandarinII hui wei weimanII huimanII edu_dur age female ccp   urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE:, dif	

esttab ochange_t2 nonagr_t2 jobup_t2 jobup2_t2 using midwest_t_corrected2.csv, ///
	se transform(ln*: exp(@) exp(@)) ///
    eqlabels("" "sd(_cons: State)" "sd(Residual)", none) ///
	varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll chi2 df_m p chi2_c df_c p_c, ///
	  layout(@ @ `""@ (@)""' @ `""@ (@)""' @)) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonumbers ///
	title(Tang laoshi's Models) ///
	mtitles("Job Change(Han vs. Uygur)" "Non-Agriculture" "Promotion (level)" "Promotion(time)")
	
	
** Hu model	
	
eststo ochange_wei: menbreg occu_change i.wei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 ||PROVINCE: ,  dif
eststo ochange_hui: menbreg occu_change i.hui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 ||PROVINCE:,  dif iterate(30)

eststo nonagr_wei:melogit nonagr i.wei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:  
eststo nonagr_hui: melogit nonagr i.hui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:  

eststo o4_wei: meologit promotion_o2 i.wei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:
eststo o4_hui: meologit promotion_o2 i.hui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:



esttab ochange_wei ochange_hui nonagr_wei nonagr_hui o4_wei o4_hui using midwest.csv, ///
	se transform(ln*: exp(@) exp(@)) ///
    eqlabels("" "sd(_cons: State)" "sd(Residual)", none) ///
	varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll chi2 df_m p chi2_c df_c p_c, ///
	  layout(@ @ `""@ (@)""' @ `""@ (@)""' @)) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonumbers ///
	title(Different Level Analysis of the Multilevel Effects) ///
	mtitles("Job Change(Han vs. Uygur)" "Job Change(Han vs. Hui)" "Non-Agriculture(Han vs. Uygur)" "Non-Agriculture(Han vs. Hui)" "Promotion(Han vs. Uygur)" "Promotion(Han vs. Hui)")	
	
eststo o1_wei: meologit promotion_o1 i.wei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1|| PROVINCE:
eststo o1_hui: meologit promotion_o1 i.hui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:
eststo o2_wei: meologit promotion_o2 i.wei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1|| PROVINCE: , dif
eststo o2_hui: meologit promotion_o2 i.hui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE: , dif
	
esttab jobup_t o2_wei o2_hui jobup2_t o1_wei o1_hui using midwest_promotion.csv, ///
	se transform(ln*: exp(@) exp(@)) ///
    eqlabels("" "sd(_cons: State)" "sd(Residual)", none) ///
	varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll chi2 df_m p chi2_c df_c p_c, ///
	  layout(@ @ `""@ (@)""' @ `""@ (@)""' @)) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonumbers ///
	title(Different Level Analysis of the Multilevel Effects) ///
	mgroups("Promotion Type" "Promotion Level", pattern(1 0 0 1 0 0)) ///
	mtitles("Promotion_Time(Tang's Model)" "Promotion_Time(Han vs. Uygur)" "Promotion_Time(Han vs. Hui)" "Promotion_Level(Tang's Model)" "Promotion_Level(Han vs. Uygur)" "Promotion_Level(Han vs. Hui)")		
	
	
eststo ochange_wei: menbreg occu_change i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp ||PROVINCE:
 
margins, dydx(hanwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 

eststo ochange_hui: menbreg occu_change i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp ||PROVINCE:,  dif

margins, dydx(hanhui) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 

 
eststo nonagr_wei:melogit nonagr i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:  

margins, dydx(hanwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 
 
eststo nonagr_hui: melogit nonagr i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:  

margins, dydx(hanhui) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 


eststo o4_wei: meologit promotion_o4 i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:
margins, dydx(hanwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 

eststo o4_hui: meologit promotion_o4 i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:
margins, dydx(hanhui) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 




esttab ochange_wei ochange_hui nonagr_wei nonagr_hui o4_wei o4_hui using midwest.rtf, ///
	se transform(ln*: exp(@) exp(@)) ///
    eqlabels("" "sd(_cons: State)" "sd(Residual)", none) ///
	varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll chi2 df_m p chi2_c df_c p_c, ///
	  layout(@ @ `""@ (@)""' @ `""@ (@)""' @)) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonumbers ///
	title(Different Level Analysis of the Multilevel Effects) ///
	mtitles("Job Change(Han vs. Uygur)" "Job Change(Han vs. Hui)" "Non-Agriculture(Han vs. Uygur)" "Non-Agriculture(Han vs. Hui)" "Promotion(Han vs. Uygur)" "Promotion(Han vs. Hui)")


/*Statistics*/	
keep if working == 1

svy: mean edu_dur, over(ethnicity3)
lincom [edu_dur]wei - [edu_dur]han 
lincom [edu_dur]hui - [edu_dur]han

svy: mean mandarin_t, over(ethnicity3)
lincom [mandarin_t]wei - [mandarin_t]han 
lincom [mandarin_t]hui - [mandarin_t]han

svy: mean occu_change, over(ethnicity3)
lincom [occu_change]wei - [occu_change]han 
lincom [occu_change]hui - [occu_change]han

svy: mean promotion_o1, over(ethnicity3)
lincom [promotion_o1]wei - [promotion_o1]han 
lincom [promotion_o1]hui - [promotion_o1]han

svy: mean promotion_o2, over(ethnicity3)
lincom [promotion_o2]wei - [promotion_o2]han 
lincom [promotion_o2]hui - [promotion_o2]han


sum occu_change nonagr promotion_o1 promotion_o2 wei hui mandarin_t edu_dur age female logincom_p urbanhukou localhukou ccp islam

tab ethnicity3 [aw=wpp],sum(mandarin_t)


	

/*Multilevel Negative Binomial*/
eststo ochange_wei: menbreg occu_change i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp ||PROVINCE:
 
margins, dydx(hanwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 

eststo ochange_hui: menbreg occu_change i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp ||PROVINCE:,  dif

margins, dydx(hanhui) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 

 
eststo nonagr_wei:melogit nonagr i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:  

margins, dydx(hanwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 
 
eststo nonagr_hui: melogit nonagr i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:  

margins, dydx(hanhui) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 


eststo o4_wei: meologit promotion_o4 i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:
margins, dydx(hanwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 

eststo o4_hui: meologit promotion_o4 i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:
margins, dydx(hanhui) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 




esttab ochange_wei ochange_hui nonagr_wei nonagr_hui o4_wei o4_hui using midwest.rtf, ///
	se transform(ln*: exp(@) exp(@)) ///
    eqlabels("" "sd(_cons: State)" "sd(Residual)", none) ///
	varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll chi2 df_m p chi2_c df_c p_c, ///
	  layout(@ @ `""@ (@)""' @ `""@ (@)""' @)) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonumbers ///
	title(Different Level Analysis of the Multilevel Effects) ///
	mtitles("Job Change(Han vs. Uygur)" "Job Change(Han vs. Hui)" "Non-Agriculture(Han vs. Uygur)" "Non-Agriculture(Han vs. Hui)" "Promotion(Han vs. Uygur)" "Promotion(Han vs. Hui)")
	
/*Compare Hui and Wei*/
gen huiwei = .
replace huiwei = 0 if hhw == 4
replace huiwei = 1 if hhw == 6

eststo ochange_hw: menbreg occu_change i.huiwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp ||PROVINCE:, iterate(30)  //not converged
margins, dydx(huiwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed)


eststo nonagr_hw:melogit nonagr i.huiwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:  
margins, dydx(huiwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 
 

eststo o4_hw: meologit promotion_o4 i.huiwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:
margins, dydx(huiwei) at(mandarinII=(1 (1) 3)) atmeans noatlegend predict(mu fixed) 



/***
SEM
***/

gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (hui -> mobility, ) (wei -> mobility,) (mobility <- _cons@PROVINCE, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o1, ) (mobility -> promotion_o2, ) (mobility -> logincom_p, ) (edu_dur -> mobility, ) (age -> mobility, ) (female -> mobility, ) (ccp -> mobility, ) (urbanhukou -> mobility, ) (localhukou -> mobility, ) (islam -> mobility, ), difficult latent(mobility ) nocapslatent 

gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (hui -> mobility, ) (wei -> mobility,) (mobility <- _cons@PROVINCE, ) (mobility -> nonagr, family(binomial) link(logit)) (edu_dur -> mobility, ) (age -> mobility, ) (female -> mobility, ) (ccp -> mobility, ) (urbanhukou -> mobility, ) (localhukou -> mobility, ) (islam -> mobility, ), difficult latent(mobility ) nocapslatent startgrid

//去掉multilevel
gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (hui -> mobility, ) (wei -> mobility, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o1, ) (mobility -> promotion_o2, ) (edu_dur -> mobility, ) (age -> mobility, ) (female -> mobility, ) (ccp -> mobility, ) (urbanhukou -> mobility, ) (localhukou -> mobility, ) (islam -> mobility, ) (occu_change -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o1 -> logincom_p, ) (promotion_o2 -> logincom_p, ), difficult iterate(20) latent(mobility ) nocapslatent

//Fixed effect

eststo sem_conditional: gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (hui -> mobility, ) (wei -> mobility, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o1, ) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (age -> mobility, ) (female -> mobility, ) (ccp -> mobility, ) (urbanhukou -> mobility, ) (localhukou-> mobility, ) (islam -> mobility, ) (i.PROVINCE -> mobility, )  (nonagr -> logincom_p,) (promotion_o1 -> logincom_p, ) (promotion_o2 -> logincom_p, )  if ethnicity3!=. & working==1, latent(mobility ) nocapslatent //converged with 46 iterations

esttab sem_conditional using sem_aftermw.rtf, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) replace nonumbers ///
	title(SEM with Fixed Effect) 

eststo sem_full_1a:gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o1, ) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (localhukou -> mobility, ) (localhukou -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o1 -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent	//21 iteration


eststo sem_full_1b:gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (hui -> mobility, ) (wei -> mobility, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o1, ) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (localhukou -> mobility, ) (localhukou -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o1 -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent	
	
esttab sem_full_1a sem_full_1b using sem_aftermw_full_1.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) replace nonumbers ///
	title(SEM with Fixed Effect) 	

* promotion_o1变成负的了,所以舍弃，实验之	

eststo sem_full_1a_2:gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (localhukou -> mobility, ) (localhukou -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent	//21 iteration

	
esttab sem_full_1a_2 using sem_aftermw_full_1_2.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) replace nonumbers ///
	title(SEM with Fixed Effect) 	

* Reverse the postion of mandarin and hui/wei
eststo sem_full_1a_3:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o1@1, ) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (localhukou -> mobility, ) (localhukou -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o1 -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent	

esttab sem_full_1a_3 using sem_aftermw_full_1_3.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) replace nonumbers ///
	title(SEM with Fixed Effect) 	

gen promotion = promotion_o1 * promotion_o2
	
eststo sem_full_1a_4:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, latent(mobility ) nocapslatent	

esttab sem_full_1a_4 using sem_aftermw_full_1_4.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) replace nonumbers ///
	title(SEM with Fixed Effect) 	

	
factor promotion_o1 promotion_o2 if ethnicity3!=. & working==1, ipf	
rotate, blank (.4)
predict betterjob

eststo sem_full_1a5:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> betterjob1, ) (mobility -> betterjob2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (nonagr -> logincom_p, ) (betterjob1 -> logincom_p, ) (betterjob2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, latent(mobility ) nocapslatent	


eststo sem_full_1a6:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> betterjob, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (occu_change -> logincom_p, ) (nonagr -> logincom_p, ) (betterjob -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent	

esttab sem_full_1a6 using sem_withfactorpromotion.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) replace nonumbers ///
	title(SEM with Fixed Effect) 


eststo sem_full_1a7:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (occu_change -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, latent(mobility ) nocapslatent		
	
esttab sem_full_1a7 using sem_sem_aftermw_full_1_7.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.05 ** 0.01 *** 0.001) replace nonumbers ///
	title(SEM with Fixed Effect) 	

eststo sem_full_1a8:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility@1 -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (occu_change -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, latent(mobility ) nocapslatent		
	
esttab sem_full_1a8 using sem_sem_aftermw_full_1_8.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.05 ** 0.01 *** 0.001) replace nonumbers ///
	title(SEM with Fixed Effect) 

eststo sem_full_1a9:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility@1 -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o1, ) (mobility -> promotion_o2, )(edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, )  (nonagr -> logincom_p, ) (promotion_o1 -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent		
	
esttab sem_full_1a9 using sem_sem_aftermw_nojobchange.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.05 ** 0.01 *** 0.001) replace nonumbers ///
	title(SEM with Fixed Effect) 
	

eststo sem_full_1c: gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> mob_result, ) (mob_result -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, latent(mobility ) nocapslatent

eststo sem_full_1d: gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (hui -> mobility, ) (wei -> mobility, ) (mobility -> mob_result, ) (mob_result -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult iterate(1600) latent(mobility ) nocapslatent


gen mod_change = occu_change * nonagr
gen mod_promo = promotion_o1 * promotion_o2

eststo sem_full_1e:  gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility -> mod_change, ) (mobility -> mod_promo, ) (mod_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (mod_promo -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent

eststo sem_full_1f: gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (hui -> mobility, ) (wei -> mobility, ) (mobility -> mod_change, ) (mobility -> mod_promo, ) (mod_change -> logincom_p, ) (edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, ) (mod_promo -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, latent(mobility ) nocapslatent intmethod(mcaghermite)

esttab sem_full_1e using sem_aftermw_full_2.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) replace nonumbers ///
	title(SEM with Fixed Effect)
	
eststo sem_full: gsem (mandarinII -> hui, ) (mandarinII -> wei, ) (mandarinII -> mobility, ) (hui -> mobility, ) (wei -> mobility, ) (mobility -> occu_change, family(nbinomial mean) link(log)) (mobility -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o1, ) (mobility -> promotion_o2, ) (occu_change -> logincom_p, ) (edu_dur -> mobility, ) (age -> mobility, ) (female -> mobility, ) (ccp -> mobility, ) (urbanhukou -> mobility, ) (localhukou -> mobility, ) (islam -> mobility, ) (i.PROVINCE -> mobility, ) (nonagr -> logincom_p, ) (promotion_o1 -> logincom_p, ) (promotion_o2 -> logincom_p, ) if ethnicity3!=. & working==1, difficult  latent(mobility ) nocapslatent

esttab sem_full using sem_aftermw.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) replace nonumbers ///
	title(SEM with Fixed Effect) 