*Dependencies
**1. grss: to track the graph
//findit grss 
//then click "grss from http://www.ats.ucla.edu/stat/stata/ado/analysis" to install
**2. coef: to graph the coefficients
//ssc install coefplot, replace
**3. estout: to output tables
//ssc install estout, replace
**4.  mdesc: to check missing data
// findit mdesc
**5.  outreg2: output without eststo
//ssc install outreg2
**6. catplot: frequencies of the categories of categorical variables.
//ssc install catplot

clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

cd "E:\Dropbox_sync\Dropbox\Instruction\Research paper\ethnic-inequality"

log using Hu_mobility.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Ethnic inequality_mobility
*AUTHOR:	Yue Hu, University of Iowa
*MACHINE:	PC Win8.1, running Stata/SE 13.1
*FILE:		"Hu_ethnicity.do"
*LOG:		"Hu_ethnicity.log"
*DATA:		CCES
*SOURCES:	2012 年中国劳动力动态调查(CLDS)
*OUTPUT:	
*		Tables/
*		Graphs/
*		LanguageSpecial.docx/
use ethnicity_mod.dta, clear
save ethnicity_mod.dta, replace
*Variable creation: occu_change, occu_immig
recode I4a_1_a1_* (99997/99999=.)
gen occu_change=0

foreach  var of varlist I4a_1_a1_* {	
	replace occu_change=occu_change+1 if `var'!=.
}

lab values hanwei hanwei
lab define hanwei 0 "Han" 1 "Wei"
catplot occu_change hanwei [aw=wpp], recast(bar) percent(hanwei)
tab occu_change hanwei, co

recode I2_16_2_* (99997/99999=.)
gen occu_immig=0
foreach  var of varlist I2_16_2_* {	
	replace occu_immig=occu_immig+1 if `var'!=.
}
catplot occu_immig hanwei [aw=wpp], recast(bar) percent(hanwei)
tab occu_immig hanwei, co

** Variables of willingness of work: hukou_change, edu_change, labor, health
/*parental hukou change*/
recode  I1_9_1 I1_9_2 I1_9_3 I1_9_4 (99997/99999=.)
gen hukou_change=.
replace hukou_change=1 if I1_9_1!=I1_9_2 | I1_9_3!=I1_9_4
replace hukou_change=0 if I1_9_1==I1_9_2!=. | I1_9_3==I1_9_4!=.

catplot hukou_change hanwei [aw=wpp], recast(bar) percent(hanwei)

/* generational education change */
recode I1_4 (7=5)(8=7)(9=8)(10=9)(11/99999=.), gen(edu_f_c)
recode edu_f_c (1=1 "no edu")(2=2 "primary")(3=3 "middle")(4/5=4 "high")  ///
(6/9=5 "college&above"), gen(edu_f_o)

recode I1_5 (7=5)(8=7)(9=8)(10=9)(11/99999=.), gen(edu_m_c)
recode edu_m_c (1=1 "no edu")(2=2 "primary")(3=3 "middle")(4/5=4 "high")  ///
(6/9=5 "college&above"), gen(edu_m_o)

replace edu_o=0 if I2_1==2
recode edu_o (4=3) (5/6=4), gen(edu_self)
replace edu_self=edu_self+1 
lab var edu_self "Identical to edu_o; for calculating edu_change"

gen edu_change=.
replace edu_change=edu_self-max(edu_f_o,edu_m_o)
svyset _n [pw=wpp]
svy: mean edu_change, over(hanwei)
lincom [edu_change]Han - [edu_change]Wei /*汉族还是比维族教育改变的多*/

rename duration edu_dur

/*Labor required*/
recode I3a1_19_1 (99996=.)
gen labor=5-I3a1_19_1

/*Health*/
gen health=6-I9_4

* SEM models	
** Subjective Mobility 
sem (hanwei -> mobility, ) (mobility -> live_hope, ) (mobility -> peace_hope, ) ///
	(mobility -> friend_hope, ) (mobility -> respect_hope, ) (mobility -> interest_hope, ) ///
	(mobility -> ability_hope, ) (duration -> mobility, ) (age -> mobility, ) ///
	(female -> mobility, ) (urbanhukou -> mobility, ) if ethnicity==1 | ethnicity==6, method(mlmv) ///
	latent(mobility ) cov( e.live_hope*e.peace_hope duration*hanwei age*hanwei ///
	age*duration female*duration urbanhukou*duration e.ability_hope*e.interest_hope) nocapslatent

sem (deficit -> deficit_live, ) (deficit -> deficit_peace, ) (deficit -> deficit_friend, ) ///
	(deficit -> deficit_respect, ) (deficit -> deficit_interest, ) (deficit -> deficit_ability, ) ///
	(duration -> deficit, ) (age -> deficit, ) (female -> deficit, ) (urbanhukou -> deficit, ) ///
	(hanwei -> deficit, ) if ethnicity==1 | ethnicity==6, method(mlmv) latent(deficit ) ///
	cov( e.deficit_live*e.deficit_peace e.deficit_live*e.deficit_interest ///
	e.deficit_peace*e.deficit_interest e.deficit_peace*e.deficit_ability ///
	e.deficit_friend*e.deficit_respect e.deficit_friend*e.deficit_interest ///
	e.deficit_friend*e.deficit_ability e.deficit_respect*e.deficit_interest ///
	e.deficit_interest*e.deficit_ability age*duration female*duration ///
	urbanhukou*duration urbanhukou*age hanwei*duration hanwei*age hanwei*urbanhukou) nocapslatent
	
* Desire to change job	
eststo: quiet reg deficit_live i.ethnicity duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_peace i.ethnicity duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_friend i.ethnicity duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_respect i.ethnicity duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_interest i.ethnicity duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_ability i.ethnicity duration age female urbanhukou welfare [pw=wpp]	

eststo: quiet reg deficit_live hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_peace hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_friend hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_respect hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_interest hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_ability hanwei duration age female urbanhukou welfare [pw=wpp]	

esttab

est clear

eststo: quiet reg deficit_live hanwei duration age female dur_hw 
eststo: quiet reg deficit_peace hanwei duration age female dur_hw 
eststo: quiet reg deficit_friend hanwei duration age female dur_hw 
eststo: quiet reg deficit_respect hanwei duration age female dur_hw 
eststo: quiet reg deficit_interest hanwei duration age female dur_hw 
eststo: quiet reg deficit_ability hanwei duration age female dur_hw 

esttab, keep(hanwei dur_hw)

est clear

eststo: quiet reg deficit_live i.occu##hanwei duration age female urbanhukou welfare
eststo: quiet reg deficit_peace i.occu##hanwei duration age female urbanhukou welfare
eststo: quiet reg deficit_friend i.occu##hanwei duration age female urbanhukou welfare
eststo: quiet reg deficit_respect i.occu##hanwei duration age female urbanhukou welfare
eststo: quiet reg deficit_interest i.occu##hanwei duration age female urbanhukou welfare
eststo: quiet reg deficit_ability i.occu##hanwei duration age female urbanhukou welfare

esttab

* Evaluation of the current job
est clear

eststo: quiet ologit occu_paid hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_safe hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_envi hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_time hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_prom hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_inte hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_coop hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_skill hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_resp hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_expr hanwei duration age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_sati hanwei duration age female urbanhukou welfare [pw=wpp]	

esttab, keep(hanwei)

est clear


gen dur_hw=duration*hanwei

eststo: quiet ologit occu_paid hanwei duration age female  dur_hw
eststo: quiet ologit occu_safe hanwei duration age female  dur_hw
eststo: quiet ologit occu_envi hanwei duration age female  dur_hw
eststo: quiet ologit occu_time hanwei duration age female  dur_hw
eststo: quiet ologit occu_prom hanwei duration age female  dur_hw
eststo: quiet ologit occu_inte hanwei duration age female  dur_hw
eststo: quiet ologit occu_coop hanwei duration age female  dur_hw
eststo: quiet ologit occu_skill hanwei duration age female  dur_hw
eststo: quiet ologit occu_resp hanwei duration age female  dur_hw
eststo: quiet ologit occu_expr hanwei duration age female  dur_hw
eststo: quiet ologit occu_sati hanwei duration age female  dur_hw

esttab, keep(hanwei dur_hw)

est clear


**Language and job

gen man_hw=mandarin*hanwei
est clear

eststo: quiet reg deficit_live hanwei mandarin man_hw duration age female urbanhukou welfare
eststo: quiet reg deficit_peace hanwei mandarin man_hw duration age female urbanhukou welfare
eststo: quiet reg deficit_friend hanwei mandarin man_hw duration age female urbanhukou welfare
eststo: quiet reg deficit_respect hanwei mandarin man_hw duration age female urbanhukou welfare
eststo: quiet reg deficit_interest hanwei mandarin man_hw duration age female urbanhukou welfare
eststo: quiet reg deficit_ability hanwei mandarin man_hw duration age female urbanhukou welfare

esttab


* New measures
/*Self education
recode I_edu (99998=.), gen(edu_c)
replace edu_c =13 if I2_5_13==1
recode edu_c (.=.5) if I2_1==1 
recode edu_c (0.5/1=1 "primary school")(2=2 "middle school")(3=3 "high school")  ///
(4/5=4 "occupational")(6/9=5 "college")(10/13=6 "graduate"), gen(edu_o)

lab var edu_o "edu6"
*/

recode I1_4 I1_5 (99997/99999=.), gen(edu_fu_c edu_mu_c)
recode edu_fu_c (1=0 "no education")(2=1 "primary school")(3=2 "middle school")(4/7=3 "high school") (8/9=4 "college")(10/11=5 "graduate"), gen(edu_fu)
recode edu_mu_c (1=0 "no education")(2=1 "primary school")(3=2 "middle school")(4/7=3 "high school") (8/9=4 "college")(10/11=5 "graduate"), gen(edu_mu)
recode edu_o (1=1 "primary school")(2=2 "middle school")(3/4=3 "high school")  ///
(5=4 "college")(6=5 "graduate"), gen(edu_self)

gen edu_mob=min(edu_self-edu_fu, edu_self-edu_mu)

** Objective Mobility
***Single DV
ologit occu_change hanwei incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp]
ologit occu_change i.hanwei##c.mandarin incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp]

foreach i of num 0/3 {
margins, dydx(hanwei) at(mandarin=(1 (1) 5)) atmeans noatlegend predict(outcome(`i'))
marginsplot, x(mandarin) recast(line) recastci(rarea) ///
  ytitle(Pr(occu_change=`i')) ylab(,grid) xlab(,grid) ///
   xtitle(Mandarin Proficiency) title("Job Change Times:`i'") ///
  legend(off) name(occu_change_mandarin_`i', replace)
}

graph combine occu_change_mandarin_0 occu_change_mandarin_1 occu_change_mandarin_2 occu_change_mandarin_3, title(Difference of Changing Jobs Frequency Between Han and Wei Conditioned by Mandarin Proficiency, size(small)) ycom

ologit occu_immig hanwei incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp]
ologit occu_immig i.hanwei##c.mandarin incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp]

/*negative interaction
ologit occu_immig incom_p c.edu_dur##c.mandarin age female urbanhukou localhukou ccp [pw=wpp]
ologit occu_change incom_p c.edu_dur##c.mandarin age female urbanhukou localhukou ccp [pw=wpp] */

mlogit occu_ty_com incom_p c.edu_dur##c.mandarin age female urbanhukou localhukou ccp [pw=wpp], base(5)



