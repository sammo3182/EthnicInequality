clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

cd "E:\Dropbox_sync\Dropbox\Instruction\Research paper\ethnic-inequality"

log using Hu_ethnicity_complement.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Ethnic inequality:generation and conflicts
*AUTHOR:	Yue Hu, University of Iowa
*MACHINE:	PC Win8.1, running Stata/SE 13.1
*FILE:		"Hu_ethnicity_ModelerWorkshop.do"
*LOG:		"Hu_ethnicity_ModelerWorkshop.log"
*DATA:		CLDS
*SOURCES:	2012 年中国劳动力动态调查
*OUTPUT:	
*		TABLES/
*		GRAPHS/

use ethnicity_mod2.dta, clear
svyset _n [pw=wpp]


/*Multilevel Negative Binomial*/
menbreg occu_change i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || COUNTY: //nosig
outreg2 using feedback_modeler_mlm.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace


menbreg occu_change i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp ||PROVINCE:  //concave for 20 iterations
 
/*xtset PROVINCE IID
xtnbreg occu_change i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp, fe //NOT USING WEIGHT*/

outreg2 using feedback_modeler_mlm.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace


melogit nonagr i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:  //concave for 20 iterations

outreg2 using feedback_modeler_mlm.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append



/*income*/
pwcorr occu_change incom_p, sig
pwcorr occu_change logincom_p, sig

mixed logincom_p occu_change hanwei c.mandarinII edu_dur age female urbanhukou localhukou ccp || PROVINCE:

outreg2 using feedback_modeler_income.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace

mixed logincom_p i.hanwei##c.mandarinII edu_dur age female urbanhukou localhukou ccp || PROVINCE:

outreg2 using feedback_modeler_income.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append



/*promotion*/
**Variable generation 
** Subjective: 请您对您目前/最后一份的工作状况进行评价(晋升机会)
gen promotion_s = 6 - I7_3_5

svy: mean promotion_s, over(hanwei)
lincom [promotion_s]Han - [promotion_s]Wei

**Subjective: 社会等级代纪变化
gen slevel_up = I7_10_1_1 - I7_10_4_1 

svy: mean slevel_up, over(hanwei)
lincom [slevel_up]Han - [slevel_up]Wei

**Objective: 工作职务变化
recode I4a_3_a6_* (90000/99999=.)

ren (I4a_3_a6_a I4a_3_a6_b I4a_3_a6_c I4a_3_a6_d I4a_3_a6_e I4a_3_a6_f I4a_3_a6_g I4a_3_a6_h I4a_3_a6_i I4a_3_a6_j) ///
	(I4a_3_a6_1 I4a_3_a6_2 I4a_3_a6_3 I4a_3_a6_4 I4a_3_a6_5 I4a_3_a6_6 I4a_3_a6_7 I4a_3_a6_8 I4a_3_a6_9 I4a_3_a6_10)

forvalue i = 1/10 {
	replace I4a_3_a6_`i' = 11 - I4a_3_a6_`i'
}
	
gen promotion_o1 = 0  //职务
replace promotion_o1 = 1 if I4a_3_a6_1 != . //至少有份非农工作
	
forvalue i = 1/9 {
	local j=`i'+1
	replace promotion_o1 = `i' + 1 if I4a_3_a6_`j' > I4a_3_a6_`i'
	gen promotion_o2_`i' = I4a_3_a6_`j' - I4a_3_a6_`i'	
}

egen promotion_o2 = rowtotal(promotion_o2_*)
replace promotion_o2 = promotion_o2 + 1 if I4a_3_a6_1 != . //至少有份非农工作

svy: mean promotion_o1, over(hanwei)
lincom [promotion_o1]Han - [promotion_o1]Wei

svy: mean promotion_o2, over(hanwei)
lincom [promotion_o2]Han - [promotion_o2]Wei


**Objective: 工作级别变化
ren (I4a_3_a5_a I4a_3_a5_b I4a_3_a5_c I4a_3_a5_d I4a_3_a5_e I4a_3_a5_f I4a_3_a5_g I4a_3_a5_h I4a_3_a5_i I4a_3_a5_j) ///
	(I4a_3_a5_1 I4a_3_a5_2 I4a_3_a5_3 I4a_3_a5_4 I4a_3_a5_5 I4a_3_a5_6 I4a_3_a5_7 I4a_3_a5_8 I4a_3_a5_9 I4a_3_a5_10)

gen promotion_o3 = 0 //级别
replace promotion_o3 = 1 if I4a_3_a5_1 != . //至少有份非农工作


recode I4a_3_a5_* (90000/99999=.)

forvalue i = 1/9 {
	replace I4a_3_a5_`i' = 11 - I4a_3_a5_`i'
}

	
forvalue i = 1/9 {
	local j=`i'+1
	replace promotion_o3 = `i' + 1 if I4a_3_a5_`j' > I4a_3_a5_`i'
	gen promotion_o4_`i' = I4a_3_a5_`j' - I4a_3_a5_`i' if I4a_3_a5_`j' > I4a_3_a5_`i'
}

egen promotion_o4 = rowtotal(promotion_o4_*)
replace promotion_o4 = promotion_o4 + 1 if I4a_3_a5_1 != . //至少有份非农工作

svy: mean promotion_o3, over(hanwei)
lincom [promotion_o3]Han - [promotion_o3]Wei

svy: mean promotion_o4, over(hanwei)
lincom [promotion_o4]Han - [promotion_o4]Wei


**Analysis
meologit promotion_s i.hanwei##c.mandarinII edu_dur age female urbanhukou localhukou ccp || PROVINCE:
outreg2 using feedback_modeler_promote.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace

meologit slevel_up i.hanwei##c.mandarinII edu_dur age female urbanhukou localhukou ccp || PROVINCE:
outreg2 using feedback_modeler_promote.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 

meologit promotion_o1 i.hanwei##c.mandarinII edu_dur age female urbanhukou localhukou ccp || PROVINCE:
outreg2 using feedback_modeler_promote.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append     //notconverged

meologit promotion_o2 i.hanwei##c.mandarinII edu_dur age female urbanhukou localhukou ccp || PROVINCE:
outreg2 using feedback_modeler_promote.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

meologit promotion_o3 i.hanwei##c.mandarinII edu_dur age female urbanhukou localhukou ccp || PROVINCE:
outreg2 using feedback_modeler_promote.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append   //notconverged

meologit promotion_o4 i.hanwei##c.mandarinII edu_dur age female urbanhukou localhukou ccp || PROVINCE:
outreg2 using feedback_modeler_promote.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append


/*SEM*/

gen hw_man = hanwei *  mandarinII

* Model 1
sem (social_mobility -> occu_change, ) (social_mobility -> nonagr, ) (social_mobility -> promotion_o2, ) (social_mobility -> promotion_o4, ) (occu_change ->logincom_p, ) (nonagr -> logincom_p, ) (promotion_o2 -> logincom_p, ) (promotion_o4 -> logincom_p, ) (mandarinII -> social_mobility, )(edu_dur -> social_mobility, ) (age -> social_mobility, ) (female -> social_mobility, ) (urbanhukou -> social_mobility, ) (localhukou -> social_mobility, ) (ccp -> social_mobility, ) (hw_man -> social_mobility, ) [pweight = wpp], method(mlmv) vce(cluster PROVINCE) noci latent(social_mobility ) nocapslatent

outreg2 using feedback_modeler_sem.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace

* Model 2
sem (social_mobility -> occu_change, ) (social_mobility -> nonagr, ) (social_mobility -> promotion_o2, ) (social_mobility -> promotion_o4, ) (social_mobility -> logincom_p, ) (mandarinII -> social_mobility, ) (edu_dur -> social_mobility, ) (age -> social_mobility, ) (female -> social_mobility, ) (urbanhukou -> social_mobility, ) (localhukou -> social_mobility, ) (ccp -> social_mobility,) (hw_man -> social_mobility, ) [pweight = wpp], method(mlmv) vce(cluster PROVINCE) noci latent(social_mobility ) cov( edu_dur*mandarinII age*mandarinII age*edu_dur female*mandarinII female*edu_dur female*age urbanhukou*mandarinII urbanhukou*edu_dur urbanhukou*age urbanhukou*female localhukou*mandarinII localhukou*edu_dur localhukou*age localhukou*female localhukou*urbanhukou ccp*mandarinII ccp*edu_dur ccp*age ccp*female ccp*urbanhukou ccp*localhukou hw_man*mandarinII hw_man*edu_dur hw_man*age hw_man*female hw_man*urbanhukou hw_man*localhukou hw_man*ccp) nocapslatent

outreg2 using feedback_modeler_sem.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

*Model 3
sem (social_mobility -> occu_change, ) (social_mobility -> nonagr, ) (social_mobility -> promotion_o2, ) (social_mobility -> logincom_p, ) (mandarinII -> social_mobility, ) (mandarinII -> hanwei, ) (edu_dur -> social_mobility, ) (edu_dur -> hanwei, ) (age -> social_mobility, ) (female -> social_mobility, ) (urbanhukou -> social_mobility, ) (localhukou -> social_mobility, ) (ccp -> social_mobility, ) (hanwei -> social_mobility, ) [pweight = wpp], method(mlmv) vce(cluster PROVINCE) noci latent(social_mobility ) nocapslatent

outreg2 using feedback_modeler_sem.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append


/*Separate ethnic groups*/
gen mandarin_ethnic = 1
replace mandarin_ethnic = 2 if ethnicity == 1
replace mandarin_ethnic = 3 if ethnicity == 3 | ethnicity == 5 | ethnicity == 9 

gen hanworse = 2- mandarin_ethnic if mandarin_ethnic != 3
gen hanbetter = mandarin_ethnic - 2 if mandarin_ethnic != 1

menbreg occu_change i.hanworse##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE: //cannot converged because of flat or discontinuous region encountered

//只有当拿掉income时才make sense
xtset PROVINCE IID

xtnbreg occu_change mandarinII edu_dur age female  urbanhukou localhukou logincom_p ccp if mandarin_ethnic == 1, fe 

outreg2 using feedback_modeler_sep.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace

xtnbreg occu_change mandarinII  edu_dur age female  urbanhukou localhukou logincom_p ccp if mandarin_ethnic == 3, fe 

outreg2 using feedback_modeler_sep.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

xtnbreg occu_change i.hanworse##c.mandarinII logincom_p edu_dur age female  urbanhukou localhukou ccp, fe 

outreg2 using feedback_modeler_sep.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

xtnbreg occu_change i.hanbetter##c.mandarinII logincom_p edu_dur age female  urbanhukou localhukou ccp, fe 

outreg2 using feedback_modeler_sep.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append


//nonagriculture

xtlogit nonagr  mandarinII edu_dur age female  urbanhukou localhukou logincom_p ccp if mandarin_ethnic == 1, fe 

outreg2 using feedback_modeler_sep2.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace

xtlogit nonagr  mandarinII  edu_dur age female  urbanhukou localhukou logincom_p ccp if mandarin_ethnic == 3, fe 

outreg2 using feedback_modeler_sep2.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

xtlogit nonagr  i.hanworse##c.mandarinII logincom_p edu_dur age female  urbanhukou localhukou ccp, fe 

outreg2 using feedback_modeler_sep2.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

xtlogit nonagr  i.hanbetter##c.mandarinII logincom_p edu_dur age female  urbanhukou localhukou ccp, fe 

outreg2 using feedback_modeler_sep2.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append


//去除income

xtnbreg occu_change i.hanworse##c.mandarinII edu_dur age female  urbanhukou localhukou ccp, fe 

outreg2 using feedback_modeler_sep.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append



