*Dependencies
**1. grss: to track the graph
//findit grss 
//then click "grss from http://www.ats.ucla.edu/stat/stata/ado/analysis" to install
**2. coef: to graph the coefficients
//ssc install coefplot, replace
**3. estout: to output tables
//ssc install estout, replace
**4.  mdesc: to check missing data
// findit mdesc
**5.  outreg2: output without eststo
//ssc install outreg2
**6. catplot: frequencies of the categories of categorical variables.
//ssc install catplot

clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

cd "E:\Dropbox_sync\Dropbox\Instruction\Research paper\ethnic-inequality"

log using Hu_lanugage.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Ethnic inequality
*AUTHOR:	Yue Hu, University of Iowa
*MACHINE:	PC Win8.1, running Stata/SE 13.1
*FILE:		"Hu_ethnicity.do"
*LOG:		"Hu_ethnicity.log"
*DATA:		CCES
*SOURCES:	2012 年中国劳动力动态调查(CLDS)
*OUTPUT:	
*		Tables/
*		Graphs/
*		LanguageSpecial.docx
use ethnicity_mod.dta, clear


svyset _n [pw=wpp]
* Compare the mean of mandarin proficiency across ethnic groups 
svy: mean mandarin, over(ethnicity)
svy: mean mandarin, over(minority)
lincom [mandarin]Han - [mandarin]Minority


graph hbar mandarin [pw = wpp], over(ethnicity, gap(*2))

**han vs. wei
svy: mean mandarin if ethnicity==1 | ethnicity==6, over(ethnicity)
lincom [mandarin]han - [mandarin]wei

graph bar mandarin if ethnicity==1 | ethnicity==6 [pw = wpp], over(ethnicity)

***control for education duration
svy: regress mandarin i.hanwei##c.duration
margins, dydx(hanwei) at(duration=(0(1)22)) vsquish 
marginsplot, recast(line) recastci(rarea) yline(0, lcolor(red)) ylab(-3(1)0, grid) xlab(0(4)22, grid) xtitle(Years of Education) ytitle(Difference between Wei and Han) title(Conditional Language Gap between Wei and Han) 


