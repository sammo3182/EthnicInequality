clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

set more off	
	
//cd "E:\OneDrive for Business\MEGAsync\Dropbox\Instruction\Research paper\EthnicInequality"

log using ethnicity_replication_analysis.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Affirmative Inaction: Education, Social Mobility and Ethnic Inequality in China
*AUTHOR:	Wenfang Tang, Yue Hu, & Shuai Jin, University of Iowa
*MACHINE:	PC Win10, running Stata/SE 14
*FILE:		"ethnicity_replication_analysis.do"
*LOG:		"ethnicity_replication_analysis.log"
*DATA:		CLDS
*SOURCES:	2012 年中国劳动力动态调查
*OUTPUT:	
*		TABLES/
*		GRAPHS/

use data_analysis.dta, clear
svyset _n [pw=wpp]

tab ethnicity
count if ethnicity != 1

/*Figure 1*/
svy: mean edu_dur, over(ethnicity)
svy: mean edu_dur

/*Such finding of ethnic equality in education...*/
use cgss2010, clear
svyset _n [pw=wpp]
recode cle_a7a (-3=.) (14=.5), gen(edu_c)
recode edu_c (5/9=5) (9 10=6) (11 12=7) (13=8), gen(edu_o)
svy:mean edu_o, over(cle_a4)

recode cle_a49-cle_a50 (-2 -3=.), gen(listen speak)
gen mandarin=listen+speak
svy: mean mandarin, over(cle_a4)


use data_analysis.dta, clear
svyset _n [pw=wpp]
/*" Second, we compare the level ... "*/
svy: mean mandarin_per, over(ethnicity)
svy: mean mandarin_per

/*Figure 3*/
svy: reg mandarin_per i.hanwei##c.edu_dur age female incom_p urbanhukou ccp

margins, dydx(hanwei) at(edu_dur=(0(1)22)) vsquish 
// vsquish: specifies that the blank space separating factor-variable terms or time-series-operated variables from other variables in the model be suppressed.
marginsplot, recast(line) recastci(rarea) yline(0, lcolor(red)) ylab(-0.8(.2)0, grid) xlab(0(4)22, grid) xtitle(Years of Education) ytitle(Difference between Uyghur and Han) title("") scheme(s1mono)

/*Table 1*/
svy: tab occu_ty_comIII hanwei, column
tab occu_ty_comIII hanwei

/*The second way to measure objective social mobility...*/
svy: mean promotion_o1, over(hanwei)
svy: mean promotion_o2, over(hanwei)

/*In the same 2012 labor survey, ...*/
svy: tab control wei, col
ologit control wei mandarin edu_dur incom_p female age urbanhukou ccp [pw=wpp] //mentioned but not shown in the paper


/*Figure 4*/
eststo sem_full:gsem (mandarin_per <- hui, ) (mandarin_per <- wei, ) (mandarin_per -> mobility, ) (mandarin_per -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility@1 -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o2, )(edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (nonagr -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent pw(wpp)

esttab sem_full using "2016-02-09_sem.csv", ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.05 ** 0.01 *** 0.001) replace nonumbers ///
	title(SEM with Fixed Effect and 0-1 Scale Mandarin)
	
/*Appendix A*/
sum mandarin_per hui wei logincom_p nonagr promotion_o2 edu_dur age female ccp urbanhukou migrant control i.PROVINCE 
sum mandarin_per hui wei logincom_p nonagr promotion_o2 edu_dur age female ccp urbanhukou migrant control i.PROVINCE if ethnicity3!=. & working==1

/*Appendix B*/
svy: mean edu_dur
svy: mean edu_dur, over(ethnicity)
lincom [edu_dur]hui - [edu_dur]han
lincom [edu_dur]wei - [edu_dur]han 

svy: mean mandarin
svy: mean mandarin, over(ethnicity)
lincom [mandarin]hui - [mandarin]han
lincom [mandarin]wei - [mandarin]han 


