*Dependencies
**1. grss: to track the graph
//findit grss 
//then click "grss from http://www.ats.ucla.edu/stat/stata/ado/analysis" to install
**2. coef: to graph the coefficients
//ssc install coefplot, replace
**3. estout: to output tables
//ssc install estout, replace
**4.  mdesc: to check missing data
// findit mdesc
**5.  outreg2: output without eststo
//ssc install outreg2
**6. catplot: frequencies of the categories of categorical variables.
//ssc install catplot

clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

cd "E:\Dropbox_sync\Dropbox\Instruction\Research paper\ethnic-inequality"

log using Hu_ethnicity.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Ethnic inequality
*AUTHOR:	Yue Hu, University of Iowa
*MACHINE:	PC Win8.1, running Stata/SE 13.1
*FILE:		"Hu_ethnicity.do"
*LOG:		"Hu_ethnicity.log"
*DATA:		CLDS
*SOURCES:	2012 年中国劳动力动态调查
*OUTPUT:	
*		DATA/
*		REGRESSIONS/

*Data management

//use ethnicity_mod.dta, clear
//save ethnicity_mod2.dta, replace
use "CLDS2012v1127ind ethnicity", clear

svyset _n [pw=wpp]
/* Demographic variables: age, female, ccp*/
replace I1_1=. if I1_1>86
ren I1_1 age

recode I1_2 (1=0 "male")(2=1 "female"), gen(female)
recode I1_6 (2/3=0 "no")(1=1 "yes"), gen(ccp)



/* Education hard to clean: edu_c, edu_o Skill*/
recode I_edu (99998=.), gen(edu_c)
replace edu_c =13 if I2_5_13==1
recode edu_c (.=.5) if I2_1==1 
recode edu_c (0.5/1=1 "primary school")(2=2 "middle school")(3=3 "high school")  ///
(4/5=4 "occupational")(6/9=5 "college")(10/13=6 "graduate"), gen(edu_o)

lab var edu_o "edu6"

gen skill=0

replace skill=1 if I2_10==1 | I3a1_11==1

/*****************************************************
edu_dur of Educatoin, and whether is still a student
*****************************************************/
recode I2_3_* (99996/99999=.) 
recode I2_4_* (99996/99999=.)

forvalue i=1/13{
		gen ed_`i'=I2_4_`i' - I2_3_`i'
}

**Detect missing end year, but have start year for next level
forvalue i=1/12{
		local j=`i'+1
		count if I2_3_`i'!=. & I2_4_`i'==. & I2_3_`j'!=. 
}

//three

** Replace them by the start year of next level 
foreach i of numlist 1/12{
	local j=`i'+1
	replace ed_`i'=I2_3_`j' - I2_3_`i' if I2_3_`i'!=. & I2_4_`i'==. & I2_3_`j'!=. 
}
		  
//someone reports very ridiculours edu_durs, like 56 years elementry school
replace ed_1=6 if ed_1 > 10 & ed_1!=.

forvalue i=2/5 {
	replace ed_`i'=3 if ed_`i' >6 & ed_`i'!=.
}

forvalue i=6/9 {
	replace ed_`i'=3 if ed_`i' >6 & ed_`i'!=.
}
	  
egen edu_dur=rowtotal(ed_1-ed_13)

replace edu_dur = 3 if edu_du ==0 & I2_1 == 1  //give 3 years to who were educated but no education duration records

hist edu_dur, freq
catplot edu_dur, recast(bar) blabel(bar, format(%3.0f))


**Alternative measure for edu_dur
g degree=0
replace degree=1 if I2_5_1==1
replace degree=2 if I2_5_2==1
replace degree=3 if I2_5_3==1
replace degree=4 if I2_5_4==1
replace degree=5 if I2_5_5==1
replace degree=6 if I2_5_6==1
replace degree=7 if I2_5_7==1
replace degree=8 if I2_5_8==1
replace degree=9 if I2_5_9==1
replace degree=10 if I2_5_10==1
replace degree=11 if I2_5_11==1
replace degree=12 if I2_5_12==1
replace degree=13 if I2_5_13==1
recode degree (4 5=3)(7=6)(8=5)(9=8)(11 12 13=10)(6=4)
recode degree (8=5)(10=6)
lab def degree 0"<primary" 1"primary" 2"jr hi" 3"srhi+tech" 4"jr college" 5"college" 6"post-grad" 
lab val degree degree
g edyr=0
replace edyr=6 if degree==1
replace edyr=9 if degree==2
replace edyr=12 if degree==3
replace edyr=14 if degree==4
replace edyr=16 if degree==5
replace edyr=18 if degree==6
tab ethnicity [aw=wpp],sum(edyr)

tab ethnicity [aw=wpp],sum(edu_dur)

collapse (mean) edyr edu_dur, by(ethnicity)

twoway bar edyr ethnicity, xla(1(1)10, valuelabel) yla(3(1)8.5,grid) name(edyr, replace)
twoway bar edu_dur ethnicity, xla(1(1)10, valuelabel) yla(3(1)8.5,grid) name(edu_dur, replace)
graph combine edyr edu_dur, cols(1)

**Whether still in school
recode I2_5_* (99996/99999=.)
gen student=1
forvalue i=1/13 {
	replace student=0 if edu_c==`i' & I2_5_`i'==1   //最高学历毕业
	replace student=0 if edu_c==`i' & I2_4_`i'!=.	//最高学历没毕业但结束了	
	replace student=. if edu_c==`i' & I2_4_`i' == . 
}
replace student=0 if edu_c==. & edu_dur==0
replace student = 0 if age > 30
replace student = 0 if occu == .

***Analysis for non-students
drop if student==1
catplot edu_dur [aw=wpp], percent missing recast(bar) blabel(bar, format(%3.1f))
catplot edu_dur minority[aw=wpp],percent recast(bar)

table ethnicity, c(mean edu_dur)
table ethnicity [aw=wpp], c(mean edu_dur)
ttest edu_dur if ethnicity==1 | ethnicity==6, by(ethnicity)

catplot edu_dur ethnicity[aw=wpp] if ethnicity==1 | ethnicity==6, ///
		percent recast(bar)

ttest edu_dur if ethnicity==1 | ethnicity==6, by(ethnicity)

ttest edu_dur, by(minority)


catplot edu_dur ethnicity[aw=wpp] if ethnicity==6, blabel(bar, format(%3.1f)) ///
		percent recast(bar)


collapse (mean) edu_dur, by(ethnicity)
twoway bar edu_dur ethnicity, xla(1(1)10, valuelabel) yla(3(1)8.5,grid)
use ethnicity_mod.dta, clear		
		
		

/* parents' education: edu_f_c, edu_f_o, edu_m_c, edu_m_o */
recode I1_4 (7=5)(8=7)(9=8)(10=9)(11/99999=.), gen(edu_f_c)
recode edu_f_c (1=1 "no edu")(2=2 "primary")(3=3 "middle")(4/5=4 "high")  ///
(6/9=5 "college&above"), gen(edu_f_o)

recode I1_5 (7=5)(8=7)(9=8)(10=9)(11/99999=.), gen(edu_m_c)
recode edu_m_c (1=1 "no edu")(2=2 "primary")(3=3 "middle")(4/5=4 "high")  ///
(6/9=5 "college&above"), gen(edu_m_o)


/* rural vs urban: urbanhukou, urbansam, localhukou */
recode I1_9_6 (1=0 "rural")(2=1 "urban"), gen(urbanhukou)
recode I1_15 (2=0 "no")(1=1 "yes"), gen(localhukou)
recode Itype (1=0 "rural")(2=1 "urban"), gen(urbansam)

/* occupation, occupation type: occu, occu_com, occu_type, employ OCCU_SATI OCCU_B*/
recode I3a_7_a (1=1 "manager")(2=2 "professional")(3=3 "white")(4=4 "service")  ///
(5=5 "agriculture")(6=6 "blue")(8=7 "maid")(7 9=8 "other"), gen(occu)

recode occu (1/3=1 "white")(6=2 "blue")(4 7=3 "service")(5=4 "agriculture")   ///
(8=5 "other"), gen(occu_com)

recode I3a_12 (1=1 "government")(2=2 "public institute")(3=3 "SOE")(4=4 "collective")  ///
(5=5 "Private")(6=6 "Foreign")(7=7 "self empolyed")(8/9=8 "agriculture")  ///
(10/99999=9 "other"), gen(occu_type)

recode occu_type (1/2=1 "gov/pub")(3/4=2 "SOE/collective")(5/6=3 "pri/for")(7=4 "self-emp")  ///
(8=5 "agriculture")(9=6 "other"), gen(occu_ty_com)

recode I3a_14 (1=1 "employee")(2=2 "employer")(3/4=3 "self-employed")(5=4 "farmer"), gen(employ)

recode I7_3_* (99998=.)
gen occu_paid=6-I7_3_1
gen occu_safe=6-I7_3_2
gen occu_envi=6-I7_3_3
gen occu_time=6-I7_3_4
gen occu_prom=6-I7_3_5
gen occu_inte=6-I7_3_6
gen occu_coop=6-I7_3_7
gen occu_skill=6-I7_3_8
gen occu_resp=6-I7_3_9
gen occu_expr=6-I7_3_10
gen occu_sati=6-I7_3_11

svy: mean occu_*, over(minority)
lincom [occu_expr]Han - [occu_expr]Minority
keep if ethnicity==1 | ethnicity==6
svy: mean occu_*, over(ethnicity)
lincom [occu_expr]han - [occu_expr]wei



gen occu_b=2-I2_13
gen occu_2011=2-I3_2

drop if student==1
catplot occu [aw=wpp] if minority==1, percent recast(bar) blabel(bar, format(%3.1f))
catplot occu [aw=wpp] if minority==0, percent recast(bar) blabel(bar, format(%3.1f))
catplot occu [aw=wpp] if ethnicity==6, percent recast(bar) blabel(bar, format(%3.1f))

catplot occu_type [aw=wpp] if minority==1, percent ///
	blabel(bar, format(%3.1f))
catplot occu_type [aw=wpp] if minority==0, percent ///
	blabel(bar, format(%3.1f)) name(hanoc, replace)
catplot occu_type [aw=wpp] if ethnicity==6, percent ///
	blabel(bar, format(%3.1f)) name(uyghoc, replace)

graph combine minoc hanoc uyghoc

/*Career Mobility*/
gen mandarin=6-I10_9

recode I1_19_* (1=0) (2=1)
recode I1_20_* (1=0) (2=1)
recode I1_21 (1=0) (2=1)
recode I1_22_* (1=0) (2=1) (99999=.)
gen welfare=sum(I1_19_1/I1_19_7)+I1_20_2+I1_20_3+I1_21+sum(I1_22_1/I1_22_3)


gen live_now=I7_4_1
gen peace_now=I7_4_2
gen friend_now=I7_4_3
gen respect_now=I7_4_4
gen interest_now=I7_4_5
gen ability_now=I7_4_6
gen live_hope=I7_5_1
gen peace_hope=I7_5_2
gen friend_hope=I7_5_3
gen respect_hope=I7_5_4
gen interest_hope=I7_5_5
gen ability_hope=I7_5_6


gen deficit_gen=sum(live_now/ability_now)-sum(live_hope/ability_hope)
gen deficit_live=live_now-live_hope
gen deficit_peace=peace_now-peace_hope
gen deficit_friend=friend_now-friend_hope
gen deficit_respect=respect_now-respect_hope
gen deficit_interest=interest_now-interest_hope
gen deficit_ability=ability_now-ability_hope

tab deficit_live [aw=wpp] if ethnicity==6
tab deficit_live [aw=wpp] if ethnicity==1
tab deficit_peace [aw=wpp] if ethnicity==6
tab deficit_peace [aw=wpp] if ethnicity==1
tab deficit_friend [aw=wpp] if ethnicity==6
tab deficit_friend [aw=wpp] if ethnicity==1
tab deficit_respect [aw=wpp] if ethnicity==6
tab deficit_respect [aw=wpp] if ethnicity==1
tab deficit_interest [aw=wpp] if ethnicity==6
tab deficit_interest [aw=wpp] if ethnicity==1
tab deficit_ability [aw=wpp] if ethnicity==6
tab deficit_ability [aw=wpp] if ethnicity==1

tab deficit_live [aw=wpp] if minority==0
tab deficit_live [aw=wpp] if minority==1
tab deficit_peace [aw=wpp] if minority==0
tab deficit_peace [aw=wpp] if minority==1
tab deficit_friend [aw=wpp] if minority==0
tab deficit_friend [aw=wpp] if minority==1
tab deficit_respect [aw=wpp] if minority==0
tab deficit_respect [aw=wpp] if minority==1
tab deficit_interest [aw=wpp] if minority==0
tab deficit_interest [aw=wpp] if minority==1
tab deficit_ability [aw=wpp] if minority==0
tab deficit_ability [aw=wpp] if minority==1


***Peasants' education for Han and wei
keep if ethnicity==1 | ethnicity==6
svy: mean edu_dur  if occu==5, over(ethnicity)
lincom [edu_dur]han - [edu_dur]wei

svy: mean edu_dur if occu_ty_com==5, over(ethnicity)
lincom [edu_dur]han - [edu_dur]wei

svy: mean age if occu==5, over(ethnicity)
lincom [age]han - [age]wei

/*SOCIAL CHANNEL FOR JOB: BESTCHAN BESTINFO*/
gen bestchan=.
replace bestchan=3 if I5b_1_1==1 | I5b_1_2==1 | I5b_1_5==1 | I5b_1_6==1 | I5b_1_7==1
replace bestchan=2 if I5b_1_3==1 | I5b_1_4==1 | I5b_1_10==1 | I5b_1_11==1 | /// 
						I5b_1_12==1 | I5b_1_13==1
replace bestchan=1 if I5b_1_8==1 | I5b_1_9==1
lab def bestchan 3 "connection" 2 "institution" 1 "self", replace
lab values bestchan bestchan

recode I5b_4 (10=1 "Irregular")(1/2 9=2 "Self") (3/6=3 "Institution") ///
			(7/8=4 "Connection"), gen(bestinfo)
			

/* annual personal income in 2011 in 10,000 */
recode I3a_6 (99997/99999=.), gen(incom_p)

replace incom_p = I3a_6_1 + I3a_6_2 if incom_p ==. & I3a_6_1 !=0 & I3a_6_2 !=0
replace incom_p = I3a_6_1 if incom_p ==. & I3a_6_1 !=0 & I3a_6_2 ==0 
replace incom_p = I3a_6_2 if incom_p ==. & I3a_6_1 ==0 & I3a_6_2 !=0 

gen logincom_p = log(incom_p)

drop if student == 1

table ethnicity [aw=wpp], c(mean incom_p)
ttest incom_p, by(minority)
ttest incom_p if ethnicity==1 | ethnicity==6, by(ethnicity)


collapse (mean) incom_p, by(ethnicity)
twoway bar incom_p ethnicity, xla(1(1)10, valuelabel) yla(3(1)8.5,grid)
use ethnicity_mod.dta, clear		
		

/* ethnicity */
recode F1_1_101_5 (1=1 "han")(2=2 "zhuang")(3=3 "man")(4 17 36=4 "hui")(5=5 "miao") ///
(6=6 "wei")(7=7 "tujia")(8=8 "yi")(9=9 "meng")(10/16 18/99999=10 "min_other"), gen(ethnicity)

recode ethnicity (1=0 "Han") (2/10=1 "Minority"), gen(minority)

gen hanwei=.
replace hanwei=1 if ethnicity==6
replace hanwei=0 if ethnicity==1


/* interpersonal trust, fair distribution, class status: trust, fair, classnow, classbe, classfu */
g trust=I7_11
g fair=I7_9
g classnow=I7_10_1_1
g classbe=I7_10_2_1 
g classfu=I7_10_3_1

/** descriptive tables *
table ethnicity, c(mean incom_p mean edu_c)

tab ethnicity occu_com, r

tab ethnicity edu_o, r

tab ethnicity occu_type, r*/

/** labor disputes **/
recode I3a1_18_1a (1=1 "yes")(.=0 "no")(2/99996=0 "no"), gen(unfairpay)
g paydelay=0
replace paydelay=1 if I3a1_18_1b==1
tab paydelay [aw=wpp] if employ==1 
*bad work environment
g badenv=0
replace badenv=1 if I3a1_18_1c==1
tab badenv [aw=wpp] if employ==1 
*over work
g overwork=0
replace overwork=1 if I3a1_18_1d==1
tab overwork [aw=wpp] if employ==1 
*injury
g injury=0 
replace injury=1 if I3a1_18_1e==1
tab injury [aw=wpp] if employ==1 

g disputenum=unfairpay+paydelay+badenv+overwork+injury
tab disputenum [aw=wpp] if employ==1

table ethnicity, c(mean disputenum)
table ethnicity, c(mean unfairpay mean paydelay mean badenv mean overwork mean injury)

/** disputes whether solved **/
recode I3a1_18_2* (4=1)(3=2)(1=3)(2=4)(99996/99999=.), gen(res_unfair  ///
res_paydelay res_badenv res_overwork res_injury)

gen res_ind = res_unfair+res_paydelay+res_badenv+res_overwork+res_injury

recode res_unfair-res_injury (0/1=0)(2/3=1), pre(b)

table ethnicity, c(mean res_unfair mean res_paydelay mean res_badenv  ///
mean res_overwork mean res_injury)
table ethnicity, c(mean bres_unfair mean bres_paydelay mean bres_badenv  ///
mean bres_overwork mean bres_injury)

/** ways chose to resolve **/
*channels: channels are coded as 
*1)group negotiation with firm; 
*2)individual negotiation with firm; 
*3)gov channels including labor unions, courts, media, ????????????????????and other organization and social groups; 
*4)group and invidual protests including strikes, petitions, walks, violence, and resignition. 

lab def channel 1"group-nego" 2"ind-nego" 3"gov" 4"protest"
recode I3a1_18_3* (1=1)(2=2)(3 4 5 10 11 12=3)(6 7 8 9=4)(99996/99999=.), gen(chan_unfair  ///
		chan_paydelay chan_badenv chan_overwork chan_injury) label(channel) 

tab I3a1_18_3a,g(c1) 
tab I3a1_18_3b,g(c2) 
tab I3a1_18_3c,g(c3) 
tab I3a1_18_3d,g(c4) 
tab I3a1_18_3e,g(c5)
recode c11-c54 (.=0)
g groupnego=c11+c21+c31+c41+c51
g indnego=c12+c22+c32+c42+c52 
g govinter=c13+c23+c33+c43+c53
g protest=c14+c24+c34+c44+c54
lab var groupnego "number of time used max=5"
lab var indnego "number of time used max=5"
lab var govinter "number of time used max=5"
lab var protest "number of time used max=5"

table ethnicity, c(mean groupnego mean indnego mean govinter mean protest)

table ethnicity, c(sum groupnego sum indnego sum govinter sum protest)


/* ethnicity - whether has disputes, whether solved */

logit unfairpay i.ethnicity edu_o age logincom_p female urbanhukou localhukou ccp ///
i.occu_ty_com if employ==1 [pw=wpp]
  outreg2 using disp_res_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace 
  
logit paydelay i.ethnicity edu_o age logincom_p female urbanhukou localhukou ccp ///
i.occu_ty_com if employ==1 [pw=wpp]
  outreg2 using disp_res_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
  
logit bres_badenv i.ethnicity edu_o age logincom_p female urbanhukou localhukou ccp  ///
i.occu_ty_com if disputenum !=0 & employ==1 
  outreg2 using disp_res_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append  

/* what channel used */
reg groupnego i.ethnicity edu_o age logincom_p female urbanhukou localhukou ccp  ///
i.occu_ty_com if disputenum !=0 & employ==1 [pw=wpp]
  outreg2 using disp_numcha_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
 
reg indnego i.ethnicity edu_o age logincom_p female urbanhukou localhukou ccp  ///
i.occu_ty_com if disputenum !=0 & employ==1 [pw=wpp]
  outreg2 using disp_numcha_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 

reg disputenum i.ethnicity edu_o age logincom_p female urbanhukou localhukou ccp  ///
i.occu_ty_com if employ==1 [pw=wpp]
  outreg2 using disp_numcha_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append  

  
/* ethnicity - education, income, occupation type */
reg edu_o i.ethnicity edu_f_c edu_m_c age female urbanhukou localhukou [pw=wpp]
 outreg2 using edu_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 

logit skill i.ethnicity edu_f_c edu_m_c age female urbanhukou localhukou [pw=wpp]
 outreg2 using edu_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

reg edu_c i.ethnicity edu_f_c edu_m_c age female urbanhukou localhukou
 outreg2 using edu_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append  
 
 

reg logincom_p i.ethnicity edu_o age female urbanhukou localhukou ccp i.occu_ty_com i.employ [pw=wpp]
 outreg2 using occu_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) ///
			side nose append 
 
mlogit occu_ty_com i.ethnicity edu_o age female urbanhukou localhukou ccp [pw=wpp]
 outreg2 using occu_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace 
 
logit occu_b i.ethnicity edu_o age female urbanhukou localhukou ccp [pw=wpp]
  outreg2 using occu_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append
  
logit occu_2011 i.ethnicity edu_o age female urbanhukou localhukou ccp i.bestchan [pw=wpp]
  outreg2 using occu_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append
  
reg occu_sati i.ethnicity edu_o age female urbanhukou localhukou ccp ///
				i.occu_ty_com i.employ [pw=wpp]
   outreg2 using occu_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

*Only Uyghur vs. Han      
keep if ethnicity==6 | ethnicity==1 //422 in the sample
recode ethnicity (1=0) (6=1), gen(wei)

/* ethnicity - whether has disputes, whether solved */

/* ethnicity - whether has disputes, whether solved */

logit unfairpay wei edu_o age logincom_p female urbanhukou localhukou ccp ///
i.occu_ty_com if employ==1 [pw=wpp]
  outreg2 using disp_res_u_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose replace 
  
logit paydelay wei edu_o age logincom_p female urbanhukou localhukou ccp ///
i.occu_ty_com if employ==1 [pw=wpp]
  outreg2 using disp_res_u_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
  
logit bres_badenv wei edu_o age logincom_p female urbanhukou localhukou ccp  ///
i.occu_ty_com if disputenum !=0 & employ==1 
  outreg2 using disp_res_u_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append  

/* what channel used */
reg groupnego wei edu_o age logincom_p female urbanhukou localhukou ccp  ///
i.occu_ty_com if disputenum !=0 & employ==1 [pw=wpp]
  outreg2 using disp_numcha_u_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
 
reg indnego wei edu_o age logincom_p female urbanhukou localhukou ccp  ///
i.occu_ty_com if disputenum !=0 & employ==1 [pw=wpp]
  outreg2 using disp_numcha_u_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 

reg disputenum wei edu_o age logincom_p female urbanhukou localhukou ccp  ///
i.occu_ty_com if employ==1 [pw=wpp]
  outreg2 using disp_numcha_u_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append  

  
/* ethnicity - education, income, occupation type */
reg edu_o wei edu_f_c edu_m_c age female urbanhukou localhukou [pw=wpp]
 outreg2 using edu_u_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 

logit skill wei edu_f_c edu_m_c age female urbanhukou localhukou [pw=wpp]
 outreg2 using edu_u_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

reg edu_c wei edu_f_c edu_m_c age female urbanhukou localhukou
 outreg2 using edu_u_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append  
 
 

reg logincom_p wei edu_o age female urbanhukou localhukou ccp i.occu_ty_com i.employ [pw=wpp]
 outreg2 using occu_u_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) ///
		 nose append 
 
mlogit occu_ty_com wei edu_o age female urbanhukou localhukou ccp [pw=wpp]
 outreg2 using occu_u_h.doc, word side alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
 
logit occu_b wei edu_o age female urbanhukou localhukou ccp [pw=wpp]
  outreg2 using occu_u_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append
  
logit occu_2011 wei edu_o age female urbanhukou localhukou ccp i.bestchan [pw=wpp]
  outreg2 using occu_u_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append
  
reg occu_sati wei edu_o age female urbanhukou localhukou ccp ///
				i.occu_ty_com i.employ [pw=wpp]
   outreg2 using occu_u_h.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append
		
 
*Occupation
mlogit occu minority incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp]
outreg2 using occueedu_dur_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append 

gen minority_edu_dur=minority*edu_dur		
mlogit occu minority incom_p edu_dur age female urbanhukou localhukou ccp ///
		minority_edu_dur [pw=wpp]
outreg2 using occueedu_dur_h2.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append 
				
mlogit occu i.ethnicity incom_p edu_dur age female urbanhukou localhukou ccp[pw=wpp]
outreg2 using occueedu_dur_h3.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append

mlogit occu minority incom_p edu_dur age female urbanhukou localhukou ccp ///
		[pw=wpp] if ethnicity==1 | ethnicity==6
outreg2 using occueedu_dur_h4.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append 

mlogit occu minority incom_p edu_dur age female urbanhukou localhukou ccp ///
		minority_edu_dur [pw=wpp] if ethnicity==1 | ethnicity==6
outreg2 using occueedu_dur_h5.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append 				

gen man_min=mandarin*minority	
est clear
eststo: mlogit occu minority edu_dur age female urbanhukou ccp  [pw=wpp]
esttab using occu.rtf, keep(minority) replace
est clear
eststo: mlogit occu hanwei edu_dur age female urbanhukou ccp  [pw=wpp]
esttab using occu.rtf, keep(hanwei) replace

est clear
quiet mlogit occu minority mandarin man_min edu_dur age female urbanhukou ccp  [pw=wpp] 
esttab using occu.rtf, keep(minority man_min) replace star(* 0.10 ** 0.05 *** 0.01) 

/*Not converged
est clear
mlogit occu hanwei mandarin man_hw edu_dur age female urbanhukou ccp  [pw=wpp] 
esttab using occu.rtf, keep(hanwei man_min) replace*/

		
/*Mobility study*/		
sem (hanwei -> mobility, ) (mobility -> live_hope, ) (mobility -> peace_hope, ) ///
	(mobility -> friend_hope, ) (mobility -> respect_hope, ) (mobility -> interest_hope, ) ///
	(mobility -> ability_hope, ) (edu_dur -> mobility, ) (age -> mobility, ) ///
	(female -> mobility, ) (urbanhukou -> mobility, ) if ethnicity==1 | ethnicity==6, method(mlmv) ///
	latent(mobility ) cov( e.live_hope*e.peace_hope edu_dur*hanwei age*hanwei ///
	age*edu_dur female*edu_dur urbanhukou*edu_dur e.ability_hope*e.interest_hope) nocapslatent

sem (deficit -> deficit_live, ) (deficit -> deficit_peace, ) (deficit -> deficit_friend, ) ///
	(deficit -> deficit_respect, ) (deficit -> deficit_interest, ) (deficit -> deficit_ability, ) ///
	(edu_dur -> deficit, ) (age -> deficit, ) (female -> deficit, ) (urbanhukou -> deficit, ) ///
	(hanwei -> deficit, ) if ethnicity==1 | ethnicity==6, method(mlmv) latent(deficit ) ///
	cov( e.deficit_live*e.deficit_peace e.deficit_live*e.deficit_interest ///
	e.deficit_peace*e.deficit_interest e.deficit_peace*e.deficit_ability ///
	e.deficit_friend*e.deficit_respect e.deficit_friend*e.deficit_interest ///
	e.deficit_friend*e.deficit_ability e.deficit_respect*e.deficit_interest ///
	e.deficit_interest*e.deficit_ability age*edu_dur female*edu_dur ///
	urbanhukou*edu_dur urbanhukou*age hanwei*edu_dur hanwei*age hanwei*urbanhukou) nocapslatent
	
	
eststo: quiet reg deficit_live i.ethnicity edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_peace i.ethnicity edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_friend i.ethnicity edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_respect i.ethnicity edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_interest i.ethnicity edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_ability i.ethnicity edu_dur age female urbanhukou welfare [pw=wpp]	

eststo: quiet reg deficit_live hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_peace hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_friend hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_respect hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_interest hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet reg deficit_ability hanwei edu_dur age female urbanhukou welfare [pw=wpp]	

esttab

est clear

eststo: quiet ologit occu_paid hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_safe hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_envi hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_time hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_prom hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_inte hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_coop hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_skill hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_resp hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_expr hanwei edu_dur age female urbanhukou welfare [pw=wpp]	
eststo: quiet ologit occu_sati hanwei edu_dur age female urbanhukou welfare [pw=wpp]	

esttab, keep(hanwei)

est clear


gen dur_hw=edu_dur*hanwei

eststo: quiet ologit occu_paid hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_safe hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_envi hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_time hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_prom hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_inte hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_coop hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_skill hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_resp hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_expr hanwei edu_dur age female  dur_hw
eststo: quiet ologit occu_sati hanwei edu_dur age female  dur_hw

esttab, keep(hanwei dur_hw)

est clear

eststo: quiet reg deficit_live hanwei edu_dur age female dur_hw 
eststo: quiet reg deficit_peace hanwei edu_dur age female dur_hw 
eststo: quiet reg deficit_friend hanwei edu_dur age female dur_hw 
eststo: quiet reg deficit_respect hanwei edu_dur age female dur_hw 
eststo: quiet reg deficit_interest hanwei edu_dur age female dur_hw 
eststo: quiet reg deficit_ability hanwei edu_dur age female dur_hw 

esttab, keep(hanwei dur_hw)

est clear

eststo: quiet reg deficit_live i.occu##hanwei edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_peace i.occu##hanwei edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_friend i.occu##hanwei edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_respect i.occu##hanwei edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_interest i.occu##hanwei edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_ability i.occu##hanwei edu_dur age female urbanhukou welfare

esttab


gen man_hw=mandarin*hanwei
est clear

eststo: quiet reg deficit_live hanwei mandarin man_hw edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_peace hanwei mandarin man_hw edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_friend hanwei mandarin man_hw edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_respect hanwei mandarin man_hw edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_interest hanwei mandarin man_hw edu_dur age female urbanhukou welfare
eststo: quiet reg deficit_ability hanwei mandarin man_hw edu_dur age female urbanhukou welfare

esttab
		
/* can't converge
mlogit occu incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if minority==1
est sto minority
outreg2 using occueedu_dur_h.doc.rtf, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Minority)

mlogit occu incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==6
est sto uyghur
outreg2 using occueedu_dur_h.doc.rtf, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Uyghur)
*/

mlogit occu_ty_com minority incom_p edu_dur age female urbanhukou localhukou ccp ///
       [pw=wpp], b(5)
outreg2 using occutyeedu_dur_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace
		
mlogit occu_ty_com minority incom_p edu_dur age female urbanhukou localhukou ccp ///
		minority_edu_dur [pw=wpp], b(5)
outreg2 using occutyeedu_dur_h2.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace 
				
mlogit occu_ty_com i.ethnicity incom_p edu_dur age female urbanhukou localhukou ///
         ccp[pw=wpp], b(5)
outreg2 using occutyeedu_dur_h3.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace
		
mlogit occu_ty_com minority incom_p edu_dur age female urbanhukou localhukou ccp ///
		[pw=wpp] if ethnicity==1 | ethnicity==6, b(5)
outreg2 using occutyeedu_dur_h4.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace 
	
mlogit occu_ty_com minority incom_p edu_dur age female urbanhukou localhukou ccp ///
		minority_edu_dur [pw=wpp] if ethnicity==1 | ethnicity==6, b(5)
outreg2 using occutyeedu_dur_h5.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace 
				

/*****
No job
******/
gen nojob=0
replace nojob=1 if I3a_7_a==9
keep if student==0

tab nojob [aw=wpp], sum(edu_dur)
tab minority [aw=wpp] if nojob==1, sum(edu_dur)

svy: mean edu_dur  if nojob==1, over(minority)
lincom [edu_dur]Han - [edu_dur]Minority

ttest edu_dur, by(nojob)

keep if ethnicity==1 | ethnicity==6
svy: mean edu_dur  if nojob==1, over(ethnicity)
lincom [edu_dur]han - [edu_dur]wei


catplot nojob minority[aw=wpp], percent recast(bar)
tab minority nojob, r
ttest nojob, by(minority)

logit nojob minority edu_dur incom_p age female urbanhukou localhukou ccp ///
	 [pw=wpp]
outreg2 using nojob_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace 	 
logit nojob minority edu_dur minority_edu_dur incom_p age female urbanhukou localhukou ccp ///
		[pw=wpp]
outreg2 using nojob_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append 		



catplot nojob ethnicity[aw=wpp] if ethnicity==1 | ethnicity==6, ///
		percent recast(bar)
tab minority nojob if ethnicity==1 | ethnicity==6, r
ttest nojob if ethnicity==1 | ethnicity==6, by(ethnicity)

logit nojob minority edu_dur incom_p age female urbanhukou localhukou ccp ///
	 [pw=wpp] if ethnicity==1 | ethnicity==6
outreg2 using nojob_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append 	 
logit nojob minority edu_dur minority_edu_dur incom_p age female urbanhukou localhukou ccp ///
		[pw=wpp] if ethnicity==1 | ethnicity==6
outreg2 using nojob_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append 		

		
/*****************************************************
The effect of education on occ and income by ethnicity
******************************************************/
*Income
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp]
est sto full
outreg2 using incomeedu_dur_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Full)
  
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if minority==0
est sto han
outreg2 using incomeedu_dur_h.rtf, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Han)

reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if minority==1
est sto minority
outreg2 using incomeedu_dur_h.rtf, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Minority)

reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==6
est sto uyghur
outreg2 using incomeedu_dur_h.rtf, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Uyghur)


coefplot full han minority uyghur, ///
 ||, keep(edu_dur) xline(0) level(90) ///
 norecycle byopts(xrescale) msize(small)  ///
 scheme(s1color)
 
**Specific ethnicities 
drop if student==1
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==1
est sto han
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace cti(Han)
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==2
est sto zhuang
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Zhuang)
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==3
est sto manchu
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Manchu)
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==4
est sto hui
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Hui)
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==5
est sto miao
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Miao)		
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==6
est sto uyghur
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Uyghur)
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==7
est sto tujia
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Tujia)
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==8
est sto yi
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Yi)
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==9
est sto mongol
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Mongolia)
reg incom_p edu_dur age female urbanhukou localhukou ccp [pw=wpp] if ethnicity==10
est sto other
outreg2 using incomeedu_dur_spe_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Other)

coefplot han zhuang manchu hui miao uyghur tujia yi mongol other, ///
 ||, keep(edu_dur) xline(0) level(90) ///
 norecycle byopts(xrescale) msize(small)  ///
 scheme(s1color)
 
coefplot han zhuang hui miao uyghur tujia yi mongol other, ///
 ||, keep(edu_dur) xline(0) level(90) ///
 norecycle byopts(xrescale) msize(small)  ///
 scheme(s1color)
 
**Interaction
gen edu_dur_minority=edu_dur*minority
reg incom_p edu_dur minority age female urbanhukou localhukou ///
		ccp [pw=wpp]
outreg2 using incomeedu_dur_h2.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace cti(Unconstraint)
		
reg incom_p edu_dur minority edu_dur_minority age female urbanhukou localhukou ///
		ccp [pw=wpp]
outreg2 using incomeedu_dur_h2.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Interact)
  
reg incom_p edu_dur minority age female urbanhukou localhukou ///
		ccp [pw=wpp] if ethnicity==1 | ethnicity==6
outreg2 using incomeedu_dur_h2.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(Wei)
		
reg incom_p edu_dur minority edu_dur_minority age female urbanhukou localhukou ///
		ccp [pw=wpp] if ethnicity==1 | ethnicity==6
outreg2 using incomeedu_dur_h2.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(InteractWei)		
		
/*CCES 2010*/
use cgss2010.dta, clear
save cgss2010.dta, replace
* Variable creation
**ethnicity: ethnicity, minority
recode cle_a4 (-3=.), gen(ethnicity)
lab def eth 1"han" 2"meng" 3"man" 4"hui" 5"zang" 6"zhuang" 7"wei" 8"other"
lab val ethnicity eth

gen minority=1
replace minority=0 if ethnicity==1
replace minority=. if ethnicity==.

*age: age
recode cle_a3a (-3=.)
gen age=2010-cle_a3a

*gender:male
gen male=2-cle_a2

*hukou: hukou, urban
recode cle_a18 (-3=.), gen(hukou)
gen urban=0
replace urban=1 if hukou==2 | hukou==2 | hukou==4 
replace urban=. if hukou==. | hukou==5

*education: edu_c, edu_o, stu_status, student
recode cle_a7a (-3=.) (14=.5), gen(edu_c)
recode edu_c (5/9=5) (9 10=6) (11 12=7) (13=8), gen(edu_o)
recode cle_a7b (-3 -1=.), gen(stu_status)
gen student=0
replace student=1 if stu_status==1

*CCP: affiliation, ccp
recode cle_a10 (-3=.), gen(affiliation)
gen ccp=0
replace ccp=1 if affiliation==1

*Income: income
recode cle_a8a (9999997/9999999=.), gen(income)

*Occupation: occu, manager
replace cle_a59j=. if cle_a59j<0
replace cle_a59j=8 if cle_a58>1 & cle_a58<5
replace cle_a59j=9 if cle_a58>5
recode cle_a59j (1=1 "government") (2=2 "business") (3=3 "public") ///
			(4=4 "social organization") (5=5 "self-employed") (6=6 "military") ///
			(7=7 "others") (8=8 "argriculture") (9=9 "nojob"), gen(occu)


recode cle_a60f (-3/-1=.) (4=2) (3=1) (2=3) (1=4), gen(manager) 

*Language: listen, speak, mandarin
recode cle_a49-cle_a50 (-2 -3=.), gen(listen speak)
gen mandarin=listen+speak

tab ethnicity [aw=weight], sum(mandarin)	


svyset _n [pw=weight]
svy: mean mandarin, over(ethnicity)
svy: mean mandarin, over(minority)
lincom [mandarin]0 - [mandarin]1
svy: mean mandarin if ethnicity==1 | ethnicity==7, over(ethnicity)
lincom [mandarin]han - [mandarin]wei


*Fairness；
recode cle_a35 (-3 -2 -1=.), gen(fairness)

tab ethnicity cle_a35 [aw=weight] if ethnicity==1, r
tab ethnicity cle_a35 [aw=weight] if ethnicity==7, r

tab ethnicity cle_l3a [aw=weight] if ethnicity==1, r
tab ethnicity cle_l3a [aw=weight] if ethnicity==7, r



/*
Han: 12% more voice in politics, 3% more freedom of speech
Wei: 27% more vocie in politics, 13% more freedom of speech
*/	

*Tolarence
recode cle_a38-cle_a41 (-3 -2 -1=.)
factor cle_a38-cle_a41, pcf factor(1)
rotate, promax horst
predict tolarence

tab ethnicity [aw=weight] if ethnicity==1 | ethnicity==7, sum(cle_a38)
tab ethnicity [aw=weight] if ethnicity==1 | ethnicity==7, sum(cle_a39)
tab ethnicity [aw=weight] if ethnicity==1 | ethnicity==7, sum(cle_a40)
tab ethnicity [aw=weight] if ethnicity==1 | ethnicity==7, sum(cle_a41)
tab ethnicity [aw=weight] if ethnicity==1 | ethnicity==7, sum(tolarence)
svy: mean tolarence if ethnicity==1 | ethnicity==7, over(ethnicity)
lincom [tolarence]han - [tolarence]wei

*Attitude
recode cle_d301-cle_d312 (-3 -2 -1=.)
factor cle_d301 cle_d302 cle_d305 cle_d306 cle_d310, pcf factor(1)
rotate, promax horst
predict trust

gen trustmil=cle_d304
gen trustgov_cen=cle_d302
gen trustgov_loc=cle_d303
gen trustjud=cle_d301
gen trustrelig=cle_d311
gen trustpol=cle_d305
gen trustleg=cle_d310

recode cle_a33 (-3 -2 -1=.), gen(intertrust)

	
*Dispute: groupact, participate
recode cle_d12a (-3=.) (2=0), gen(groupact)
recode cle_d12c (-3/-2=.), gen(part_temp)
gen participation=7-part_temp




/*************************************************************
Effect of language on income, occupation, nojob, and attitudes
**************************************************************/
drop if student==1

reg income minority edu_o age male urban ccp [pw=weight]
reg income minority mandarin edu_o age male urban ccp [pw=weight]
outreg2 using language_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace cti(income_mino)	
gen mand_mino=mandarin*minority
gen edu_mino=edu_o*minority
reg income minority mandarin mand_mino edu_o age male urban ccp [pw=weight]
outreg2 using language_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(income_miIn)	

reg income minority edu_o age male urban ccp [pw=weight] if ethnicity==1 | ethnicity==7
reg income minority mandarin edu_o age male urban ccp [pw=weight] if ethnicity==1 | ethnicity==7
outreg2 using language_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(income_wei)	
reg income minority mandarin mand_mino edu_o age male urban ccp [pw=weight] if ethnicity==1 | ethnicity==7
outreg2 using language_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose append cti(income_weiIn)	
		
mlogit occu minority mandarin edu_o age male urban ccp [pw=weight]
outreg2 using language_occu_h.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace		
mlogit occu minority mandarin mand_mino edu_o age male urban ccp [pw=weight]

		
mlogit occu minority mandarin edu_o age male urban ccp [pw=weight] if ethnicity==1 | ethnicity==7
outreg2 using language_occu_h2.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace			
mlogit occu minority mandarin mand_mino edu_o age male urban ccp [pw=weight] if ethnicity==1 | ethnicity==7

mlogit occu mandarin edu_o age male urban ccp [pw=weight] if minority==0
outreg2 using language_occu_h3.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace	
mlogit occu mandarin edu_o age male urban ccp [pw=weight] if minority==1
outreg2 using language_occu_h4.doc, word alpha(.01,.05, .1) sym (***,**,*) ///
		dec(3) nose replace	

ologit manager mandarin edu_o age male urban ccp [pw=weight] if minority==1
ologit manager mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7


gen trustgov_bi=.
replace trustgov_bi=0 if trustgov_cen==4
replace trustgov_bi=1 if trustgov_cen==5
gen man_edu=mandarin*edu_o
logit trustgov_bi mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7

logit trustgov_bi mandarin edu_o age male urban ccp man_edu [pw=weight] if ethnicity==7 //insig


ologit trustgov_loc mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
ologit trustpol mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
ologit trustmil mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
ologit trustleg mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
ologit trustjud mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
ologit fairness mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
reg tolarence mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7


ologit trustgov_loc mandarin edu_o age male urban ccp man_edu [pw=weight] if ethnicity==7
ologit trustpol mandarin edu_o age male urban ccp man_edu [pw=weight] if ethnicity==7
ologit trustmil mandarin edu_o age male urban ccp man_edu [pw=weight] if ethnicity==7
ologit trustleg mandarin edu_o age male urban ccp man_edu [pw=weight] if ethnicity==7
ologit trustjud mandarin edu_o age male urban ccp man_edu [pw=weight] if ethnicity==7
ologit fairness mandarin edu_o age male urban ccp man_edu [pw=weight] if ethnicity==7
reg tolarence mandarin edu_o age male urban ccp man_edu [pw=weight] if ethnicity==7
	
/* 没什么重要结果
reg fairness mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
reg tolarence mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
reg trust mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
reg intertrust mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
reg trustmil mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7
reg trustrelig mandarin edu_o age male urban ccp [pw=weight] if ethnicity==7

reg fairness mandarin  age [pw=weight] if ethnicity==7
reg tolarence mandarin  age [pw=weight] if ethnicity==7
reg trust mandarin  age [pw=weight] if ethnicity==7
reg intertrust mandarin  age [pw=weight] if ethnicity==7
reg trustmil mandarin  age [pw=weight] if ethnicity==7
reg trustrelig mandarin  age [pw=weight] if ethnicity==7

reg fairness mandarin edu_o age male urban ccp [pw=weight] if minority==1
reg tolarence mandarin edu_o age male urban ccp [pw=weight] if minority==1
reg trust mandarin edu_o age male urban ccp [pw=weight] if minority==1
reg intertrust mandarin edu_o age male urban ccp [pw=weight] if minority==1
reg trustmil mandarin edu_o age male urban ccp [pw=weight] if minority==1
reg trustrelig mandarin edu_o age male urban ccp [pw=weight] if minority==1
*/

recode mandarin (2=0) (3/10=1), gen(mandarin_b)
/* Doesn't work, either.
reg fairness mandarin_b edu_o age male urban ccp [pw=weight] if ethnicity==7
reg tolarence mandarin_b edu_o age male urban ccp [pw=weight] if ethnicity==7
reg trust mandarin_b edu_o age male urban ccp [pw=weight] if ethnicity==7
reg intertrust mandarin_b edu_o age male urban ccp [pw=weight] if ethnicity==7
reg trustmil mandarin_b edu_o age male urban ccp [pw=weight] if ethnicity==7
reg trustrelig mandarin_b edu_o age male urban ccp [pw=weight] if ethnicity==7
*/
mdesc participation manager listen speak mandarin occu trust trustmil trustrelig intertrust fairness tolarence

