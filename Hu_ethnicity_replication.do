clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

set more off	
	
cd "E:\Dropbox_sync\Dropbox\Instruction\Research paper\EthnicInequality"

log using Hu_ethnicity_aftermidwest.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Affirmative Inaction: Education, Social Mobility and Ethnic Inequality in China
*AUTHOR:	Wenfang Tang, Yue Hu, & Shuai Jin, University of Iowa
*MACHINE:	PC Win10, running Stata/SE 14
*FILE:		"Hu_ethnicity_aftermidwest.do"
*LOG:		"Hu_ethnicity_aftermidwest.log"
*DATA:		CLDS
*SOURCES:	2012 年中国劳动力动态调查
*OUTPUT:	
*		TABLES/
*		GRAPHS/

use ethnicity_mod2.dta, clear
svyset _n [pw=wpp]

/*Figure 1*/
svy: mean edu_dur, over(ethnicity)

/*" Second, we compare the level ... "*/
gen mandarin_perc = (mandarinII - 1)/4 * 100
svy: mean mandarin_perc, over(ethnicity)

/*Figure 3*/
svy: reg mandarin i.hanwei##c.edu_dur 
margins, dydx(hanwei) at(edu_dur=(0(1)22)) vsquish 
// vsquish: specifies that the blank space separating factor-variable terms or time-series-operated variables from other variables in the model be suppressed.
marginsplot, recast(line) recastci(rarea) yline(0, lcolor(red)) ylab(-3(1)0, grid) xlab(0(4)22, grid) xtitle(Years of Education) ytitle(Difference between Uyghur and Han) title("") scheme(s1mono)

/*Table 1*/
svy: tab occu_ty_comIII hanwei, column

/*Figure 4*/
eststo sem_full:gsem (mandarinII <- hui, ) (mandarinII <- wei, ) (mandarinII -> mobility, ) (mandarinII -> logincom_p, ) (hui -> mobility, ) (hui -> logincom_p, ) (wei -> mobility, ) (wei -> logincom_p, ) (mobility@1 -> nonagr, family(binomial) link(logit)) (mobility -> promotion_o2, )(edu_dur -> mobility, ) (edu_dur -> logincom_p, ) (age -> mobility, ) (age -> logincom_p, ) (female -> mobility, ) (female -> logincom_p, ) (ccp -> mobility, ) (ccp -> logincom_p, ) (urbanhukou -> mobility, ) (urbanhukou -> logincom_p, ) (migrant -> mobility, ) (migrant -> logincom_p, ) (islam -> mobility, ) (islam -> logincom_p, )  (nonagr -> logincom_p, ) (promotion_o2 -> logincom_p, ) (i.PROVINCE -> mobility, ) (i.PROVINCE -> logincom_p, ) if ethnicity3!=. & working==1, difficult latent(mobility ) nocapslatent 	
	
esttab sem_full using sem_sem_aftermw_nojobchange.csv, ///
	se varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll) /// @ represents the position of a parameter.
	star(* 0.05 ** 0.01 *** 0.001) replace nonumbers ///
	title(SEM with Fixed Effect) 
	
/*Appendix A*/
sum mandarinII hui wei logincom_p nonagr promotion_o2 edu_dur age female ccp urbanhukou migrant islam i.PROVINCE if ethnicity3!=. & working==1

/*Appendix B*/
svy: mean edu_dur, over(ethnicity)
lincom [edu_dur]man - [edu_dur]han 
lincom [edu_dur]hui - [edu_dur]han
lincom [edu_dur]wei - [edu_dur]han 


svy: mean mandarinII, over(ethnicity)
lincom [mandarinII]man - [mandarinII]han 
lincom [mandarinII]hui - [mandarinII]han
lincom [mandarinII]wei - [mandarinII]han 
