clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

cd "E:\Dropbox_sync\Dropbox\Instruction\Research paper\ethnic-inequality"

log using Hu_ethnicity_complement.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Ethnic inequality:generation and conflicts
*AUTHOR:	Yue Hu, University of Iowa
*MACHINE:	PC Win8.1, running Stata/SE 13.1
*FILE:		"Hu_ethnicity_complement.do"
*LOG:		"Hu_ethnicity_complement.log"
*DATA:		CLDS
*SOURCES:	2012 年中国劳动力动态调查
*OUTPUT:	
*		TABLES/
*		REGRESSIONS/

*Data management

use ethnicity_mod.dta, clear

* General attention: urbanhukou
svyset _n [pw=wpp]
sort urbanhukou
by urbanhukou: tab hanwei

svy: mean edu_dur if urbanhukou == 1, over(hanwei)
svy: mean edu_dur if urbanhukou == 0, over(hanwei)

svy: mean occu_change if urbanhukou == 1, over(hanwei)
lincom [occu_change]Han - [occu_change]Wei
svy: mean occu_change if urbanhukou == 0, over(hanwei)
lincom [occu_change]Han - [occu_change]Wei

* Generation variables: edu_f_c, edu_m_c
 
/* ethnicity - education, income, occupation type */
reg edu_o hanwei edu_f_c edu_m_c age female urbanhukou localhukou ccp [pw=wpp]
 outreg2 using h_complement.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
reg edu_o hanwei edu_f_c edu_m_c age female localhukou ccp [pw=wpp] if urbanhukou == 1
	outreg2 using h_complement.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
reg edu_o hanwei edu_f_c edu_m_c age female localhukou ccp [pw=wpp] if urbanhukou == 0
	outreg2 using h_complement.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 

reg edu_o hanwei edu_f_c edu_m_c incom_p age female urbanhukou localhukou ccp [pw=wpp]
 outreg2 using h_complement.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
reg edu_o hanwei edu_f_c edu_m_c incom_p age female localhukou ccp [pw=wpp] if urbanhukou == 1
	outreg2 using h_complement.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 
reg edu_o hanwei edu_f_c edu_m_c incom_p age female localhukou ccp [pw=wpp] if urbanhukou == 0 
	outreg2 using h_complement.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append 	

//MI for income	
set seed 313
mi set wide
mi register imputed incom_p
mi register regular occu_change hanwei edu_dur age female urbanhukou localhukou ccp
mi impute mvn incom_p = occu_change hanwei edu_dur age female urbanhukou localhukou ccp, ///
			add(100) force


mi estimate, post: reg edu_o hanwei edu_f_c edu_m_c incom_p age female urbanhukou localhukou ccp [pw=wpp]
mi estimate, post: reg edu_o hanwei edu_f_c edu_m_c incom_p age female localhukou ccp [pw=wpp] if urbanhukou == 1
mi estimate, post: reg edu_o hanwei edu_f_c edu_m_c incom_p age female localhukou ccp [pw=wpp] if urbanhukou == 0 
 
 
logit skill hanwei edu_f_c edu_m_c age female urbanhukou localhukou [pw=wpp] //skill: if the respondent has any professional certification (专业技术资格证书 or 国家标准职业资格认证书). 
 outreg2 using h_complement.doc, word alpha(.01,.05, .1) sym (***,**,*) dec(3) nose append

* Social mobility in work
recode I4a_1_a1_* (99997/99999=.)
gen occu_change=0

foreach  var of varlist I4a_1_a1_* {	
	replace occu_change=occu_change+1 if `var'!=.
}
