clear all
program drop _all
capture log close
set matsize 768
 quietly log
  local logon = r(status)
  if "`logon'" == "on" {
   log close
    }

set more off	
	
//cd "E:\OneDrive for Business\MEGAsync\Dropbox\Instruction\Research paper\EthnicInequality"

log using Hu_ethnicity_replication_data.log, append			        /*Log created; "append" command will keep adding to the log 
													  in the future*/
display "$S_TIME  $S_DATE"							/*Display date and time*/

*PURPOSE:	Ethnic inequality:generation and conflicts
*AUTHOR:	Wenfang Tang, Yue Hu, & Shuai Jin, University of Iowa
*MACHINE:	PC Win10, running Stata/SE 14.0
*FILE:		"ethnicity_replication_data.do"
*LOG:		"ethnicity_replication_data.log"
*DATA:		CLDS
*SOURCES:	2012 年中国劳动力动态调查
*OUTPUT:	
*		TABLES/
*		GRAPHS/

//use "E:\OneDrive for Business\MEGAsync\Dropbox\Instruction\Research paper\EthnicInequality\CLDS2012v1127ind ethnicity.dta", clear

svyset _n [pw=wpp]

gen mandarin=6-I10_9

gen mandarin_per = (mandarin - 1)/4

recode F1_1_101_5 (1=1 "han")(2=2 "zhuang")(3=3 "man")(4 17 36=4 "hui")(5=5 "miao") ///
(6=6 "wei")(7=7 "tujia")(8=8 "yi")(9=9 "meng")(10/16 18/99999=10 "min_other"), gen(ethnicity)

g han = 1 if F1_1_101_5 == 1
g hui = 1 if F1_1_101_5 == 4
g wei = 1 if F1_1_101_5 == 6

replace han = 0 if F1_1_101_5 != 1
replace hui = 0 if F1_1_101_5 != 4
replace wei = 0 if F1_1_101_5 != 6

gen hanwei=.
replace hanwei=0 if han==1
replace hanwei=1 if wei==1

g ethnicity3=.
replace ethnicity3=1 if han==1
replace ethnicity3=2 if hui==1
replace ethnicity3=3 if wei==1
lab def ethnicity3 1"han" 2"hui" 3"wei"
lab val ethnicity3 ethnicity3

recode I3a_6 (99997/99999=.), gen(incom_p)

replace incom_p = I3a_6_1 + I3a_6_2 if incom_p ==. & I3a_6_1 !=0 & I3a_6_2 !=0
replace incom_p = I3a_6_1 if incom_p ==. & I3a_6_1 !=0 & I3a_6_2 ==0 
replace incom_p = I3a_6_2 if incom_p ==. & I3a_6_1 ==0 & I3a_6_2 !=0 

gen logincom_p = log(incom_p)


recode I3a_12 (1=1 "government")(2=2 "public institute")(3=3 "SOE")(4=4 "collective")  ///
(5=5 "Private")(6=6 "Foreign")(7=7 "self empolyed")(8/9=8 "agriculture")  ///
(10/99999=9 "other"), gen(occu_type)

recode occu_type (1/2=1 "gov/pub")(3/4=2 "SOE/collective")(5/6=3 "pri/for")(7=4 "self-emp")  ///
(8=5 "agriculture")(9=6 "other"), gen(occu_ty_com)

recode occu_type (8 = 0)(. = .)(else = 1), gen(nonagr)
recode occu_type (8 = 1 "Agriculture")(1/4 = 3 "Public")(5/7 = 2 "Non-public")(9 = 4 "Others"), gen(occu_ty_comIII)


recode I4a_3_a6_* (90000/99999=.)

ren (I4a_3_a6_a I4a_3_a6_b I4a_3_a6_c I4a_3_a6_d I4a_3_a6_e I4a_3_a6_f I4a_3_a6_g I4a_3_a6_h I4a_3_a6_i I4a_3_a6_j) ///
	(I4a_3_a6_1 I4a_3_a6_2 I4a_3_a6_3 I4a_3_a6_4 I4a_3_a6_5 I4a_3_a6_6 I4a_3_a6_7 I4a_3_a6_8 I4a_3_a6_9 I4a_3_a6_10)

forvalue i = 1/10 {
	replace I4a_3_a6_`i' = 11 - I4a_3_a6_`i'
}
	
gen promotion_o1 = 0  //职务
replace promotion_o1 = 1 if I4a_3_a6_1 != . //至少有份非农工作
	
forvalue i = 1/9 {
	local j=`i'+1
	replace promotion_o1 = `i' + 1 if I4a_3_a6_`j' > I4a_3_a6_`i'
	gen promotion_o2_`i' = I4a_3_a6_`j' - I4a_3_a6_`i'	
}

egen promotion_o2 = rowtotal(promotion_o2_*)
replace promotion_o2 = promotion_o2 + 1 if I4a_3_a6_1 != . //至少有份非农工作



recode I2_3_* (99996/99999=.) 
recode I2_4_* (99996/99999=.)

forvalue i=1/13{
		gen ed_`i'=I2_4_`i' - I2_3_`i'
}

**Detect missing end year, but have start year for next level
forvalue i=1/12{
		local j=`i'+1
		count if I2_3_`i'!=. & I2_4_`i'==. & I2_3_`j'!=. 
}

** Replace them by the start year of next level 
foreach i of numlist 1/12{
	local j=`i'+1
	replace ed_`i'=I2_3_`j' - I2_3_`i' if I2_3_`i'!=. & I2_4_`i'==. & I2_3_`j'!=. 
}
		  
//someone reports very ridiculours edu_durs, like 56 years elementry school
replace ed_1=6 if ed_1 > 10 & ed_1!=.

forvalue i=2/5 {
	replace ed_`i'=3 if ed_`i' >6 & ed_`i'!=.
}

forvalue i=6/9 {
	replace ed_`i'=3 if ed_`i' >6 & ed_`i'!=.
}
	  
egen edu_dur=rowtotal(ed_1-ed_13)

replace edu_dur = 3 if edu_du ==0 & I2_1 == 1  //give 3 years to who were educated but no education duration records


ren I1_1 age
recode I1_2 (1=0 "male")(2=1 "female"), gen(female)
recode I1_6 (2/3=0 "no")(1=1 "yes"), gen(ccp)
recode I1_9_6 (1=0 "rural")(2=1 "urban"), gen(urbanhukou)

recode I1_15 (2=0 "no")(1=1 "yes"), gen(localhukou)
gen migrant = 1 - localhukou


gen islam =.
replace islam = 1 if I7_1 == 6
replace islam = 0 if I7_1 != 6 & I7_1 != .


g working=0 
replace working=1 if I3a_13==1


recode I7_7_1 (1=5)(2=4)(3=3)(4=2)(5=1), gen(control)

tab1 mandarin hui wei logincom_p nonagr promotion_o2 edu_dur age female ccp urbanhukou migrant islam control

save data_analysis.dta, replace
