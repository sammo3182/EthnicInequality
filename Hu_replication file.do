/*Tang laoshi's model*/

//Variable Creation 
g mandarin_t=(5-I10_9)/4*100

g working=0 
replace working=1 if I3a_13==1

g han = 1 if F1_1_101_5 == 1
g hui = 1 if F1_1_101_5 == 4
g wei = 1 if F1_1_101_5 == 6

replace han = 0 if F1_1_101_5 != 1
replace hui = 0 if F1_1_101_5 != 4
replace wei = 0 if F1_1_101_5 != 6

g ethnicity3=.
replace ethnicity3=1 if han==1
replace ethnicity3=2 if hui==1
replace ethnicity3=3 if wei==1
lab def ethnicity3 1"han" 2"hui" 3"wei"
lab val ethnicity3 ethnicity3

g weiman=wei*mandarin_t
g huiman=hui*mandarin_t

/*****************************************************
edu_dur of Educatoin, and whether is still a student
*****************************************************/
recode I2_3_* (99996/99999=.) 
recode I2_4_* (99996/99999=.)

forvalue i=1/13{
		gen ed_`i'=I2_4_`i' - I2_3_`i'
}

**Detect missing end year, but have start year for next level
forvalue i=1/12{
		local j=`i'+1
		count if I2_3_`i'!=. & I2_4_`i'==. & I2_3_`j'!=. 
}


** Replace them by the start year of next level 
foreach i of numlist 1/12{
	local j=`i'+1
	replace ed_`i'=I2_3_`j' - I2_3_`i' if I2_3_`i'!=. & I2_4_`i'==. & I2_3_`j'!=. 
}
		  
//someone reports very ridiculours edu_durs, like 56 years elementry school
replace ed_1=6 if ed_1 > 10 & ed_1!=.

forvalue i=2/5 {
	replace ed_`i'=3 if ed_`i' >6 & ed_`i'!=.
}

forvalue i=6/9 {
	replace ed_`i'=3 if ed_`i' >6 & ed_`i'!=.
}
	  
egen edu_dur=rowtotal(ed_1-ed_13)

replace edu_dur = 3 if edu_du ==0 & I2_1 == 1  //give 3 years to who were educated but no education duration records

/**********************
Jobup: 工作级别变化
**********************/
recode I4a_3_a6_* (90000/99999=.)

ren (I4a_3_a6_a I4a_3_a6_b I4a_3_a6_c I4a_3_a6_d I4a_3_a6_e I4a_3_a6_f I4a_3_a6_g I4a_3_a6_h I4a_3_a6_i I4a_3_a6_j) ///
	(I4a_3_a6_1 I4a_3_a6_2 I4a_3_a6_3 I4a_3_a6_4 I4a_3_a6_5 I4a_3_a6_6 I4a_3_a6_7 I4a_3_a6_8 I4a_3_a6_9 I4a_3_a6_10)

forvalue i = 1/10 {
	replace I4a_3_a6_`i' = 11 - I4a_3_a6_`i'
}
	
gen promotion_o1 = 0  //职务
replace promotion_o1 = 1 if I4a_3_a6_1 != . //至少有份非农工作
	
forvalue i = 1/9 {
	local j=`i'+1
	replace promotion_o1 = `i' + 1 if I4a_3_a6_`j' > I4a_3_a6_`i'
	gen promotion_o2_`i' = I4a_3_a6_`j' - I4a_3_a6_`i'	
}

egen promotion_o2 = rowtotal(promotion_o2_*)
replace promotion_o2 = promotion_o2 + 1 if I4a_3_a6_1 != . //至少有份非农工作


/***************************************
annual personal income in 2011 in 10,000 
****************************************/
recode I3a_6 (99997/99999=.), gen(incom_p)

replace incom_p = I3a_6_1 + I3a_6_2 if incom_p ==. & I3a_6_1 !=0 & I3a_6_2 !=0
replace incom_p = I3a_6_1 if incom_p ==. & I3a_6_1 !=0 & I3a_6_2 ==0 
replace incom_p = I3a_6_2 if incom_p ==. & I3a_6_1 ==0 & I3a_6_2 !=0 

gen logincom_p = log(incom_p)


/**************************************
rural vs urban: urbanhukou, localhukou 
***************************************/
recode I1_9_6 (1=0 "rural")(2=1 "urban"), gen(urbanhukou)
recode I1_15 (2=0 "no")(1=1 "yes"), gen(localhukou)


//Regressive Analysis

eststo ochange_t: menbreg occu_change mandarin_t hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE: ,dif
eststo nonagr_t: melogit nonagr mandarin_t hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE: 
eststo jobup_t: meologit promotion_o4 mandarin_t hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE:





eststo ochange_t: menbreg occu_change mandarinII hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE: ,dif
eststo nonagr_t: melogit nonagr mandarinII hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE: 
eststo jobup_t: meologit promotion_o4 mandarinII hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE:

meologit promotion_o3 mandarinII hui wei weiman huiman edu_dur age female ccp logincom_p urbanhukou localhukou islam if ethnicity3!=. & working==1 ||PROVINCE:


esttab ochange_t nonagr_t jobup_t using midwest_t.csv, ///
	se transform(ln*: exp(@) exp(@)) ///
    eqlabels("" "sd(_cons: State)" "sd(Residual)", none) ///
	varlabels(,elist(weight:_cons "{break}{hline @width}")) ///
	stats(N ll chi2 df_m p chi2_c df_c p_c, ///
	  layout(@ @ `""@ (@)""' @ `""@ (@)""' @)) /// @ represents the position of a parameter.
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace nonumbers ///
	title(Tang laoshi's Models) ///
	mtitles("Job Change(Han vs. Uygur" "Non-Agriculture" "Promotion")


** Hu model	
***With orignial mandarin scale
	
eststo ochange_wei: menbreg occu_change i.wei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 ||PROVINCE: ,  dif
eststo ochange_hui: menbreg occu_change i.hui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 ||PROVINCE:,  dif iterate(30)

eststo nonagr_wei:melogit nonagr i.wei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:  
eststo nonagr_hui: melogit nonagr i.hui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:  

eststo o4_wei: meologit promotion_o4 i.wei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:
eststo o4_hui: meologit promotion_o4 i.hui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp islam if ethnicity3!=. & working==1 || PROVINCE:


***With 1-3 mandarin scale	

eststo ochange_wei: menbreg occu_change i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp ||PROVINCE:
eststo ochange_hui: menbreg occu_change i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp ||PROVINCE:,  dif

eststo nonagr_wei:melogit nonagr i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:  
eststo nonagr_hui: melogit nonagr i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:  

eststo o4_wei: meologit promotion_o4 i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:
eststo o4_hui: meologit promotion_o4 i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:

eststo o3_wei: meologit promotion_o3 i.hanwei##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:
eststo o3_hui: meologit promotion_o3 i.hanhui##c.mandarinII edu_dur age female logincom_p urbanhukou localhukou ccp || PROVINCE:

//Descriptive Statistics
keep if working == 1

svy: mean mandarin_t, over(ethnicity3) //equivalent to tab ethnicity3 [aw=wpp],sum(mandarin_t)
lincom [mandarin_t]wei - [mandarin_t]han  //ttest
lincom [mandarin_t]hui - [mandarin_t]han

svy: mean edu_dur, over(ethnicity3)
lincom [edu_dur]wei - [edu_dur]han 
lincom [edu_dur]hui - [edu_dur]han

svy: mean occu_change, over(ethnicity3)
lincom [occu_change]wei - [occu_change]han 
lincom [occu_change]hui - [occu_change]han

svy: mean promotion_o4, over(ethnicity3)
lincom [promotion_o4]wei - [promotion_o4]han 
lincom [promotion_o4]hui - [promotion_o4]han

sum occu_change nonagr promotion_o4 wei hui mandarin_t edu_dur age female logincom_p urbanhukou localhukou ccp islam
